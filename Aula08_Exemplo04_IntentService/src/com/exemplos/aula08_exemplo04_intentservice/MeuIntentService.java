package com.exemplos.aula08_exemplo04_intentservice;

import java.net.MalformedURLException;
import java.net.URL;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class MeuIntentService extends IntentService {

    public MeuIntentService() {
        super("Nome_MeuIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            int result = downloadFile(new URL("http://www.amazon.com/somefile.pdf"));
            Log.d("IntentService", "Downloaded " + result + " bytes");
  
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private int downloadFile(URL url) {
        try {
            //---Simula um tempo para o download do arquivo---
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return 101;
    }
    @Override
	public void onDestroy() {
		super.onDestroy();
		Toast.makeText(this, "Servi�o Encerrado", Toast.LENGTH_LONG).show();
	}
}