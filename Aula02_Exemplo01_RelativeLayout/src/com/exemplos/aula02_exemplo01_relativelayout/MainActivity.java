package com.exemplos.aula02_exemplo01_relativelayout;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	EditText txtUsuario;
	EditText txtSenha;
	CheckBox chkSenha;
	Button btnLogin;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtSenha = (EditText) findViewById(R.id.txtSenha);
        chkSenha =(CheckBox)findViewById(R.id.chkSenha);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        
        
        chkSenha.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				 if(chkSenha.isChecked()){
			        	Toast.makeText(getApplicationContext(), "Marcado!", Toast.LENGTH_LONG).show();
			        }
			        else{
			        	Toast.makeText(getApplicationContext(), "Desmarcado!", Toast.LENGTH_LONG).show();
			        }
			}
        	
        });
       
        btnLogin.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String usuario = txtUsuario.getText().toString();
				String senha = txtSenha.getText().toString();
				
				String msg = usuario + "\n" + senha;
				Toast.makeText(getApplicationContext(), "Bot�o Login!" + "\n" + msg , Toast.LENGTH_LONG).show();
			}
		});
        
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       switch (item.getItemId()) {
       case R.id.menu_settings:
    	   startActivity(new Intent(getApplicationContext(),Activity2.class));
          return true;
       }
       return false;
    }
}
