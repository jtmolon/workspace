package com.exemplos.aula01_calc02;

import android.os.Bundle;
import android.app.Activity;
import android.widget.TextView;

public class Temperatura extends Activity {
	TextView lblFarenheit;
	TextView lblKelvin;
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	float valor = 0;
    	float farenheit=0;
    	float kelvin=0;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.temperaturas);
        
        lblFarenheit = (TextView) findViewById(R.id.lblRes1);
        lblKelvin = (TextView) findViewById(R.id.lblRes2);
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
        	valor = Float.parseFloat(extras.getString("temperatura"));
        	farenheit = (float) (valor * 1.8) + 32;
        	kelvin = valor + (float) 273.15;
        	
        	lblFarenheit.setText(Float.toString(farenheit));
        	lblKelvin.setText(Float.toString(kelvin));
        }
        
    }

}
