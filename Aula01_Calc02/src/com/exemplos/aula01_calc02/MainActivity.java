package com.exemplos.aula01_calc02;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	EditText txtTemperatura;
	Button btnCalculo;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		txtTemperatura = (EditText) findViewById(R.id.txtTemperatura);
		btnCalculo = (Button) findViewById(R.id.btnConverter);
		btnCalculo.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				String temperatura = txtTemperatura.getText().toString();
				if (txtTemperatura.getText().length() == 0) {
					Toast.makeText(getApplicationContext(), "Entre um n�mero valido",Toast.LENGTH_LONG).show();
					return;
				}
				else{
    				Intent intencao = new Intent(getApplicationContext(), Temperatura.class);
    				Bundle extras = new Bundle();
    				extras.putString("temperatura",temperatura);
    				intencao.putExtras(extras);
    				startActivity(intencao);
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
