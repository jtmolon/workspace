package com.exemplos.aula02_exemplo02_listview;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.view.View;


public class MainActivity extends Activity {
	 /** Called when the activity is first created. */
		ListView lista;
		
		String contatos[]={"Ana", "Andr�","Henrique","Ricardo","Lisiane"};
		ArrayAdapter<String> adapter;
		
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_main);
	        
	        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contatos); 
	        lista = (ListView) findViewById(R.id.listView1);
	        lista.setAdapter(adapter);
	        
	        lista.setOnItemClickListener(new OnItemClickListener(){

				public void onItemClick(AdapterView<?> adapter, View view, int pos, long id) {
					//Toast.makeText(ExemploListViewActivity.this,"Selecionado " + contatos[pos],Toast.LENGTH_SHORT).show();
					String itemSelecionado =(String) (lista.getItemAtPosition(pos)); 
							
					if(itemSelecionado != null){					
						AlertDialog.Builder dialogo = new AlertDialog.Builder(MainActivity.this);
						dialogo.setTitle("Contato Selecionado");
						dialogo.setMessage(itemSelecionado);
						dialogo.setNeutralButton("OK", null);
						dialogo.show();
					}
				}
	        });        
	    }  
	}