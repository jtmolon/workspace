São duas aplicações:

QuestoesCopa_JoaoMolon
	-Cadastro de questões para um quiz da copa.

QuizCopa_JoaoMolon
	-Quiz propriamente dito. Carrega as questões cadastradas no outro app, utilizando ContentProvider. Possui um banco próprio também, para registro dos históricos de pontuação. Trata-se, na verdade, de um quiz genérico, dependendo do tipo de questão cadastrada.
