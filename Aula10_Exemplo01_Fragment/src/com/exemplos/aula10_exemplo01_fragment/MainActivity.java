package com.exemplos.aula10_exemplo01_fragment;

import android.os.Bundle;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.view.Menu;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fragManager = getFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
        	Fragment2 fragment2 = new Fragment2();
            fragTransaction.replace(android.R.id.content, fragment2);
        }
        else{
        	Fragment1 fragment1 = new Fragment1();
            fragTransaction.replace(android.R.id.content, fragment1);
        }
        fragTransaction.commit();    

    }   

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
