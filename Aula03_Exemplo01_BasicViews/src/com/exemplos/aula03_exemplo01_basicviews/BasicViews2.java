package com.exemplos.aula03_exemplo01_basicviews;

import android.widget.ProgressBar;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;

public class BasicViews2 extends Activity 
{
	private static int progress = 0;
    private ProgressBar progressBar;
    private int progressStatus = 0;
    private Handler handler = new Handler();
 
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basicviews2);
 
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
 
        //---do some work in background thread---
        new Thread(new Runnable(){
            public void run(){
                //---do some work here---
                while (progressStatus < 10){
                    progressStatus = doSomeWork();
                }
 
                //---hides the progress bar---
                handler.post(new Runnable(){
                    public void run(){
                        //---0 - VISIBLE; 4 - INVISIBLE; 8 - GONE---
                        progressBar.setVisibility(8);
                    }
                });
            }    
 
            //---do some long lasting work here---
            private int doSomeWork(){
                try {
                    //---simulate doing some work---
                    Thread.sleep(500);
                }
                catch (InterruptedException e){
                    e.printStackTrace();
                }
                return ++progress;
            }
 
        }).start();  
    }
}