package com.exemplos.aula03_exemplo01_basicviews;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;
 
public class BasicViews3 extends Activity 
{
	private static int progress = 0;
    private ProgressBar progressBar;
    private int progressStatus = 0;
    private Handler handler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basicviews3);
        
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        progressBar.setMax(200);//barra vai at� 200 (valor m�ximo)
        
        new Thread(new Runnable(){
            public void run(){
                while (progressStatus < 200){ //100 � a metade da barra
                    progressStatus = doSomeWork();
 
                    //---Update the progress bar---
                    handler.post(new Runnable(){
                        public void run() {
                            progressBar.setProgress(progressStatus);
                        }
                    });
                }
                //---hides the progress bar---
                handler.post(new Runnable(){
                    public void run(){
                        //---0 - VISIBLE; 4 - INVISIBLE; 8 - GONE---
                        progressBar.setVisibility(8);
                    }
                });
            }    
 
            private int doSomeWork(){
                try{
                    //---simulate doing some work---
                    Thread.sleep(500);
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
                return ++progress;
            }
        }).start();

    }  
}