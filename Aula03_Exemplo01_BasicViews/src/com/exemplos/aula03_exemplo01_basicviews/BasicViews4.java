package com.exemplos.aula03_exemplo01_basicviews;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
 
public class BasicViews4 extends Activity
{
    String[] presidents = 
    {
            "Dwight D. Eisenhower",
            "John F. Kennedy",
            "Lyndon B. Johnson",
            "Richard Nixon",
            "Gerald Ford",
            "Jimmy Carter",
            "Ronald Reagan",
            "George H. W. Bush",
            "Bill Clinton",
            "George W. Bush",
            "Barack Obama"
    };
 
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basicviews4);
 
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, presidents);
 
        AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.txtCountries);

        //O m�todo setThreshold determina a quantidade de caracteres que o usu�rio deve digitarpara o autocomplete funcionar
        textView.setThreshold(3);
        textView.setAdapter(adapter);         
    }
}