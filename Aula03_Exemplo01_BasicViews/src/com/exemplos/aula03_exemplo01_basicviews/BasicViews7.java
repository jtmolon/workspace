package com.exemplos.aula03_exemplo01_basicviews;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
 
public class BasicViews7 extends Activity 
{
    String[] presidentes;
 
    Spinner s1;
 
    @Override    
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basicviews7);
 
        s1 = (Spinner) findViewById(R.id.spinner1);
 
        presidentes = getResources().getStringArray(R.array.array_presidentes);
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
            android.R.layout.simple_spinner_item, presidentes);
        
        //Para fazer com que a lista apresente bot�es de r�dio
   //    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
   //             android.R.layout.simple_spinner_dropdown_item, presidentes);
 
        s1.setAdapter(adapter);
        s1.setOnItemSelectedListener(new OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> arg0, 
            View arg1, int arg2, long arg3) 
            {
                int index = s1.getSelectedItemPosition();
                Toast.makeText(getBaseContext(), 
                    "Voc� selecionou o presidente : " + presidentes[index], 
                    Toast.LENGTH_SHORT).show();                
            }
 
            public void onNothingSelected(AdapterView<?> arg0) {}
        });
    }
}