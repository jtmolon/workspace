package com.exemplos.aula03_exemplo01_basicviews;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       switch (item.getItemId()) {
       case R.id.menu01:
    	   startActivity(new Intent(this,BasicViews1.class));
           return true;
       case R.id.menu02:
 	       startActivity(new Intent(this,BasicViews2.class));
           return true;
       case R.id.menu03:
	       startActivity(new Intent(this,BasicViews3.class));
           return true;
       case R.id.menu04:
	       startActivity(new Intent(this,BasicViews4.class));
           return true;
       case R.id.menu05:
	       startActivity(new Intent(this,BasicViews5.class));
           return true;
       case R.id.menu06:
	       startActivity(new Intent(this,BasicViews6.class));
           return true;
       case R.id.menu07:
	       startActivity(new Intent(this,BasicViews7.class));
          return true;
    }
       return false;
    }  
}