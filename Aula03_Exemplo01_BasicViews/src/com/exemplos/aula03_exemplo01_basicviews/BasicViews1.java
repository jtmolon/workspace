package com.exemplos.aula03_exemplo01_basicviews;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class BasicViews1 extends Activity 
{
	Button btnOpen;
	Button btnSave;
	ImageButton btnImg;
	CheckBox checkBox;
	CheckBox checkEstrela;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basicviews1);
        
        
        //---Button view---
 
        btnOpen = (Button) findViewById(R.id.btnOpen);
        btnOpen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	DisplayToast("Voc� clicou no bot�o Open");
            }
        });
 
        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() 
        {
            public void onClick(View v) {
                DisplayToast("Voc� clicou no bot�o Save");
            }
        });
        
        btnImg = (ImageButton) findViewById(R.id.btnImg1);
        btnImg.setOnClickListener(new View.OnClickListener() 
        {
            public void onClick(View v) {
                DisplayToast("Voc� clicou no bot�o com Imagem");
            }
        });
 
        //---CheckBox---
        checkBox = (CheckBox) findViewById(R.id.chkAutosave);
        checkBox.setOnClickListener(new View.OnClickListener() 
        {
            public void onClick(View v) {
                if (((CheckBox)v).isChecked()) 
                    DisplayToast("CheckBox est� marcado!");
                else
                    DisplayToast("CheckBox est� desmarcado!");            
            }
        });
        checkEstrela = (CheckBox) findViewById(R.id.star);
        checkEstrela.setOnClickListener(new View.OnClickListener() 
        {
            public void onClick(View v) {
                if (((CheckBox)v).isChecked()) 
                    DisplayToast("CheckBox Estrela est� marcado!");
                else
                    DisplayToast("CheckBox Estrela est� desmarcado!");            
            }
        });
        
 
        //---RadioButton---
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.rdbGp1);        
        radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() 
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                //---displays the ID of the RadioButton that is checked---
                //DisplayToast(Integer.toString(checkedId));
                
                int checkedRadioButton = group.getCheckedRadioButtonId();
                String radioButtonSelected = "";
                switch (checkedRadioButton) {
                  case R.id.rdb1 : radioButtonSelected = "Opcao 1";
                  				   break;
                  case R.id.rdb2 : radioButtonSelected = "Opcao 2";
                		           break;
                }
                DisplayToast(radioButtonSelected);
            }
        });
        
        /*
         * RadioGroup radioGroup = (RadioGroup) findViewById(R.id.rdbGp1);
        radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb1 = (RadioButton) findViewById(R.id.rdb1);
                if (rb1.isChecked()) {
                    DisplayToast(�Option 1 checked!�);
                } else {
                    DisplayToast(�Option 2 checked!�);
                }
            }
        });
         */
         
 
        //---ToggleButton---
        ToggleButton toggleButton = (ToggleButton) findViewById(R.id.toggle1);
        toggleButton.setOnClickListener(new View.OnClickListener() 
        {
            public void onClick(View v) {
               if (((ToggleButton)v).isChecked())
                    DisplayToast("Bot�o Toggle est� em On");
               else
                   DisplayToast("Bot�o Toggle est� em Off");
            }
        });

    }

    private void DisplayToast(String msg)
    {
         Toast.makeText(getBaseContext(), msg, 
                 Toast.LENGTH_SHORT).show();        
    }    
}