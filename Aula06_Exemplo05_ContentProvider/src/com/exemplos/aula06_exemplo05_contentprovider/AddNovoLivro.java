package com.exemplos.aula06_exemplo05_contentprovider;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class AddNovoLivro extends Activity{
   private long idLinha; 
   private EditText txtTitulo;
   private EditText txtEditora;
   private EditText txtISBN;
   private Button btnSalvar;

   @Override
   public void onCreate(Bundle savedInstanceState){
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_addlivros); 

      txtTitulo = (EditText) findViewById(R.id.txtTitulo);
      txtEditora = (EditText) findViewById(R.id.txtEditora);
      txtISBN = (EditText) findViewById(R.id.txtISBN);
      
      Bundle extras = getIntent().getExtras(); // get Bundle of extras

      // Se h� extras, usa os valores para preencher a tela
      if (extras != null){
         idLinha = extras.getLong("idLinha");
         txtTitulo.setText(extras.getString("titulo"));  
         txtEditora.setText(extras.getString("editora"));  
         txtISBN.setText(extras.getString("isbn"));  
      } // end if
    
      btnSalvar = (Button) findViewById(R.id.btnSalvar);
      btnSalvar.setOnClickListener(salvarLivroButtonClicked);
   } 

   OnClickListener salvarLivroButtonClicked = new OnClickListener(){
      public void onClick(View v){
         if (txtTitulo.getText().length() != 0){
            AsyncTask<Object, Object, Object> salvaLivroTask = new AsyncTask<Object, Object, Object>(){
                  @Override
                  protected Object doInBackground(Object... params){
                     salvaLivro(); // Salva o livro na base de dados
                     return null;
                  } // end method doInBackground
      
                  @Override
                  protected void onPostExecute(Object result){
                     finish(); // Fecha a atividade
                  } 
               };
               
            // Salva o livro no BD usando uma thread separada
            salvaLivroTask.execute((Object[]) null); 
         } // end if
         else {
            // Cria uma caixa de di�logo 
            AlertDialog.Builder builder = new AlertDialog.Builder(AddNovoLivro.this);
            builder.setTitle(R.string.tituloErro); 
            builder.setMessage(R.string.mensagemErro);
            builder.setPositiveButton(R.string.botaoErro, null); 
            builder.show(); 
         } 
      } 
   }; 

   // Salva o livro na base de dados
   private void salvaLivro(){ 
      try{
         if (getIntent().getExtras() == null){
        	 ContentValues dados = new ContentValues();
             dados.put(LivrosProvider.KEY_TITULO, txtTitulo.getText().toString());
             dados.put(LivrosProvider.KEY_EDITORA,txtEditora.getText().toString());
             dados.put(LivrosProvider.KEY_ISBN, txtISBN.getText().toString());  
             ContentResolver cr = getContentResolver();
             Uri uri = cr.insert(LivrosProvider.CONTENT_URI, dados);
         } 
         else{
        	 ContentValues dados = new ContentValues();
             dados.put(LivrosProvider.KEY_TITULO, txtTitulo.getText().toString());
             dados.put(LivrosProvider.KEY_EDITORA,txtEditora.getText().toString());
             dados.put(LivrosProvider.KEY_ISBN, txtISBN.getText().toString());        
             ContentResolver cr = getContentResolver();
             Uri uri = Uri.parse("content://"+ LivrosProvider.PROVIDER_NAME + "/livros/" + idLinha);            
             cr.update(uri, dados, null,null);
         }
      }catch(SQLException e){
    	 e.printStackTrace();
      }
   } 
} 
