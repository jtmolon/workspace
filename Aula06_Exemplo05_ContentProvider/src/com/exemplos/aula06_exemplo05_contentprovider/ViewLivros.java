package com.exemplos.aula06_exemplo05_contentprovider;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

public class ViewLivros extends Activity 
{
   private long idLinha; 
   private TextView lblTitulo; 
   private TextView lblEditora;
   private TextView lblISBN;

   @Override
   public void onCreate(Bundle savedInstanceState){
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_livros);

      lblTitulo = (TextView) findViewById(R.id.lblTitulo);
      lblEditora = (TextView) findViewById(R.id.lblEditora);
      lblISBN = (TextView) findViewById(R.id.lblISBN);

      Bundle extras = getIntent().getExtras();
      idLinha = extras.getLong(MainActivity.LINHA_ID); 
   } 
   
   @Override
   protected void onResume(){
      super.onResume();
      new CarregaLivroTask().execute(idLinha);
   } 
   
   // Executa a consulta em uma thead separada
   private class CarregaLivroTask extends AsyncTask<Long, Object, Cursor>{
      @Override
      protected Cursor doInBackground(Long... params){
    	  Uri livros = Uri.parse("content://"+ LivrosProvider.PROVIDER_NAME + "/livros"+idLinha);//params[0]
	 	  ContentResolver cr = getContentResolver();
	 	  Cursor cursor = cr.query(livros, null, null, null, null);
	      return cursor; //retorna o livro 
      } 
      // Usa o Cursor retornado do m�todo doInBackground
      @Override
      protected void onPostExecute(Cursor result) {
         super.onPostExecute(result);
         result.moveToFirst(); 
         
         int tituloIndex = result.getColumnIndex("titulo");
         int editoraIndex = result.getColumnIndex("editora");
         int isbnIndex = result.getColumnIndex("isbn");

         lblTitulo.setText(result.getString(tituloIndex));
         lblEditora.setText(result.getString(editoraIndex));
         lblISBN.setText(result.getString(isbnIndex));
         result.close(); 
      } 
   } 
      
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
	   getMenuInflater().inflate(R.menu.activity_viewlivros, menu);
       return true; 
   } 
   
   @Override
   public boolean onOptionsItemSelected(MenuItem item){
      switch (item.getItemId()){
         case R.id.editItem:
            Intent addEditLivro = new Intent(this, AddNovoLivro.class);
            
            addEditLivro.putExtra(MainActivity.LINHA_ID, idLinha);
            addEditLivro.putExtra("titulo", lblTitulo.getText());
            addEditLivro.putExtra("editora", lblEditora.getText());
            addEditLivro.putExtra("isbn", lblISBN.getText());
 
            startActivity(addEditLivro); 
            return true;
         case R.id.deleteItem:
            deleteLivro(); 
            return true;
         default:
            return super.onOptionsItemSelected(item);
      } 
   } 
   
   private void deleteLivro(){
      
      AlertDialog.Builder builder = new AlertDialog.Builder(ViewLivros.this);

      builder.setTitle(R.string.confirmaTitulo); 
      builder.setMessage(R.string.confirmaMensagem); 

      // provide an OK button that simply dismisses the dialog
      builder.setPositiveButton(R.string.botao_delete,
         new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int button){

               // create an AsyncTask that deletes the contact in another 
               // thread, then calls finish after the deletion
               AsyncTask<Long, Object, Object> deleteTask =
                  new AsyncTask<Long, Object, Object>(){
                     @Override
                     protected Object doInBackground(Long... params){
                    	 try{
                    		 ContentResolver cr = getContentResolver();
                    		 Uri uri = Uri.parse("content://"+ LivrosProvider.PROVIDER_NAME + "/livros/" + idLinha); //params[0]           
                             cr.delete(uri, null, null);
                    	 }
                         catch(SQLException e){
                        	 e.printStackTrace();
                         }
                        return null;
                     } // end method doInBackground

                     @Override
                     protected void onPostExecute(Object result){
                        finish(); 
                     } // end method onPostExecute
                  }; // end new AsyncTask

               // execute the AsyncTask to delete contact at rowID
               deleteTask.execute(new Long[] { idLinha });               
            } // end method onClick
         } // end anonymous inner class
      ); // end call to method setPositiveButton
      
      builder.setNegativeButton(R.string.botao_cancel, null);
      builder.show(); // display the Dialog
   } // end method deleteContact

} 