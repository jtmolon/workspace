package com.exemplos.aula10_exemplo02_fragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class Menu_Fragment extends Fragment {
	Button btn1;
	Button btn2;
	Button btn3;
	Button btn4;
	
	OnItemClickedCallBack callBack; //objeto da interface callback
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.menu,container,false);
		btn1 = (Button) view.findViewById(R.id.menu1);
		btn2 = (Button) view.findViewById(R.id.menu2);
		btn3 = (Button) view.findViewById(R.id.menu3);
		btn4 = (Button) view.findViewById(R.id.menu4);
		
		
		btn1.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View v) {
    			callBack.onItemClicked(v.getId()); //sinaliza que foi clicado o bot�o
    		}
    	});
		btn2.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View v) {
    			callBack.onItemClicked(v.getId()); //sinaliza que foi clicado o bot�o
    		}
    	});
		btn3.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View v) {
    			callBack.onItemClicked(v.getId()); //sinaliza que foi clicado o bot�o
    		}
    	});
		btn4.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View v) {
    			callBack.onItemClicked(v.getId()); //sinaliza que foi clicado o bot�o
    		}
    	});
		return view;
	}
	
	/* OBRIGAT�RIO - Garante que a Activity que ir� carregar o fragment implementa a interface de callback
	 * onAttach() � um m�todo do ciclo de vida do Fragment. � executado ANTES do onCreateView()
	 * � executado quando o fragmento � vinculado ("atachado") � atividade em que ele ser� executado. 
	 */
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callBack = (OnItemClickedCallBack) activity;
		} catch (ClassCastException e) {
			Log.e("Menu_Fragment", activity.toString() + " DEVE IMPLEMENTAR OnItemClickedCallBack");
		}
	}

}
