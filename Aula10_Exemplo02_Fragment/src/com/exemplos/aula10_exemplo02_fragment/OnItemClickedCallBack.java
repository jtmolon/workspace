package com.exemplos.aula10_exemplo02_fragment;

public interface OnItemClickedCallBack  {
	public void onItemClicked(int position);
}
