package com.exemplos.aula10_exemplo02_fragment;

import android.os.Bundle;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Fragment;
import android.util.Log;
import android.view.Menu;

public class MainActivity extends Activity implements OnItemClickedCallBack {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/* Como o menu sempre existir�, ele � instanciado fora da condi��o */
		Menu_Fragment menu = new Menu_Fragment();
		
		if (findViewById(R.id.main) != null) {
			/* Se for acessado de um smartphone o espa�o main existir�.
			 * Adiciona o menu no �nico espa�o */
			FragmentManager fm = getFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			
			ft.add(R.id.main,menu);
			ft.commit();
		} 
		else{
			/** Se for acessado de um tablet o espa�o main n�o existir�, existir� o menu e content */
			if (findViewById(R.id.content) != null){
				FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				
				ft.add(R.id.menu,menu);
				ft.add(R.id.content, new Conteudo_Fragment());
				ft.commit(); 
			}
		}		
	}

	@Override
	public void onItemClicked(int menuItem) {
		// TODO Auto-generated method stub
		Fragment novoFragment = null;

		switch (menuItem) {
		case R.id.menu1: // Fragment que deve ser aberto quando for o menu 1
			Log.d("MENU","Estou aqui");
			novoFragment = getNewFragment(1);
			Log.d("MENU","Estou aqui 2");
			break;

		case R.id.menu2: // Fragment que deve ser aberto quando for o menu 2
			novoFragment = getNewFragment(2); 
			break;

		case R.id.menu3: // Fragment que deve ser aberto quando for o menu 3
			novoFragment = getNewFragment(3); 
			break;
			
		case R.id.menu4: // Fragment que deve ser aberto quando for o menu 4
			novoFragment = getNewFragment(4); 
			break;
		}
		
		if (novoFragment != null) {
			FragmentManager fm = getFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();

			if (findViewById(R.id.main) != null) {
				/* Se for acessado de um smartphone o espa�o main existir�
				   Adiciona o fragment com o novo conte�do no �nico espa�o */
				
				ft.replace(R.id.main, novoFragment);
				ft.addToBackStack(null);
				
			} else if (findViewById(R.id.content) != null) {
				/* Se for acessado de um tablet o espa�o main n�o existir�, existir� o menu e content
				   Coloca o fragment com o novo conte�do do lado direito */
				ft.replace(R.id.content, novoFragment); 
			}
			ft.commit();
		}
	}
	
	/** 
	 * M�todo auxiliar que faz a gera��o do ContentFragment com o conte�do adequado ao menu clicado 
	 * Na pr�tica � prov�vel que voc� n�o utilize um m�todo como este pois voc� ter� fragments espec�ficos 
	 */
	private Fragment getNewFragment(int menu) {
		Conteudo_Fragment fragment = new Conteudo_Fragment();
		
		/* Adiciona um argumento indicando qual menu foi clicado */
		Bundle args = new Bundle();
		args.putInt("menu", menu);
		fragment.setArguments(args);
		
		/* Retorna o fragment criado */
		return fragment;
	}
}