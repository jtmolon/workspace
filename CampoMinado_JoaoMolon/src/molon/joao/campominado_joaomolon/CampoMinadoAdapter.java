package molon.joao.campominado_joaomolon;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class CampoMinadoAdapter extends BaseAdapter {

	private Context context;
	private List<Casa> casaList; 
	
	public CampoMinadoAdapter(Context context, List<Casa> casaList) {
		this.context = context;
		this.casaList = casaList;
	}

	@Override
	public int getCount() {
		return this.casaList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.casaList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;        
		if (convertView == null) {             
			imageView = new ImageView(this.context);            
			imageView.setLayoutParams(new GridView.LayoutParams(20, 20));            
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP); //escala a imagem - largura e altura           
			imageView.setPadding(2, 2, 2, 2);  //espa�o entre as imagens      
		} else {            
			imageView = (ImageView) convertView;        
		}        
		Casa casa = this.casaList.get(position);
		imageView.setImageResource(casa.getImagem());        
		return imageView;	
	}

}
