package molon.joao.campominado_joaomolon;

import java.util.Random;

public class CampoMinado {

	private int dimensao;
	private int bombas;
	private Casa[][]campo;
	
	public CampoMinado(int dimensao, int bombas) {
		this.dimensao = dimensao;
		this.bombas = bombas;
		this.campo = new Casa[dimensao][dimensao];
		for (int i = 0; i < this.dimensao; i++) {
			for (int j = 0; j < this.dimensao; j++) {
				this.campo[i][j] = new Casa(0, false, i, j);
			}
		}
		this.distribuirBombas();
		this.setarNumeros();
	}
	
	public int getDimensao() {
		return dimensao;
	}

	public void setDimensao(int dimensao) {
		this.dimensao = dimensao;
	}

	public int getBombas() {
		return bombas;
	}

	public void setBombas(int bombas) {
		this.bombas = bombas;
	}

	public Casa[][] getCampo(){
		return this.campo;
	}
	
	public void distribuirBombas(){
		Random random = new Random();        
        
        for (int i = 0; i< this.bombas; i++){
            int linha = random.nextInt(this.dimensao);
            int coluna = random.nextInt(this.dimensao);
            while (this.campo[linha][coluna].getNumero() != 0){
                linha = random.nextInt(this.dimensao);
                coluna = random.nextInt(this.dimensao);
            }            
            this.campo[linha][coluna].setNumero(9);
        }
        
	}
	
	public void setarNumeros(){
		
	    for (int i = 0; i < this.dimensao; i++){
	    	for (int j = 0; j < this.dimensao; j++){
	    		if (this.campo[i][j].getNumero() == 0){
	    			int bombas = 0;
	    			boolean colunaAntes = j - 1 >= 0;
	    			boolean colunaDepois = j + 1 <= this.dimensao - 1;
	    			boolean linhaAntes = i - 1 >= 0;
	    			boolean linhaDepois = i + 1 <= this.dimensao - 1;
	    			if (colunaAntes && this.campo[i][j - 1].getNumero() == 9)
	    				bombas += 1;
	    			if (colunaDepois && this.campo[i][j + 1].getNumero() == 9)
	    				bombas += 1;
	    			if (linhaAntes) {
	    				if (colunaAntes && this.campo[i - 1][j - 1].getNumero() == 9)
	    					bombas += 1;
	    				if (this.campo[i - 1][j].getNumero() == 9)
	    					bombas += 1;
	    				if (colunaDepois && this.campo[i - 1][j + 1].getNumero() == 9)
	    					bombas += 1;
	    			}
	    			if (linhaDepois){
	    				if (colunaAntes && this.campo[i + 1][j - 1].getNumero() == 9)
	    					bombas += 1;
	    				if (this.campo[i + 1][j].getNumero() == 9)
	    					bombas += 1;
	    				if (colunaDepois && this.campo[i + 1][j + 1].getNumero() == 9)
	    					bombas += 1;
	    			}
	    			this.campo[i][j].setNumero(bombas);
	    		}
	    	}
	    }
	}
	
	public void abrePosicao(int linha, int coluna){
        boolean colunaAntes = coluna - 1 >= 0;
        boolean colunaDepois = coluna + 1 <= this.dimensao - 1;
        boolean linhaAntes = linha - 1 >= 0;
        boolean linhaDepois = linha + 1 <= this.dimensao - 1;
        
        if (colunaAntes && this.campo[linha][coluna - 1].getNumero() != 9 && !this.campo[linha][coluna - 1].isVisivel() && !this.campo[linha][coluna].isBandeira()){
            this.campo[linha][coluna - 1].setVisivel(true);
            if (this.campo[linha][coluna - 1].getNumero() == 0)
                this.abrePosicao(linha, coluna - 1);
        }
        
        if (colunaDepois && this.campo[linha][coluna + 1].getNumero() != 9 && !this.campo[linha][coluna + 1].isVisivel() && !this.campo[linha][coluna].isBandeira()){
            this.campo[linha][coluna + 1].setVisivel(true);
            if (this.campo[linha][coluna + 1].getNumero() == 0)
                this.abrePosicao(linha, coluna + 1);
        }
        
        if (linhaAntes){
        	
            if (colunaAntes && this.campo[linha - 1][coluna - 1].getNumero() != 9 && !this.campo[linha - 1][coluna - 1].isVisivel() && !this.campo[linha - 1][coluna - 1].isBandeira()){
                this.campo[linha - 1][coluna - 1].setVisivel(true);
                if (this.campo[linha - 1][coluna - 1].getNumero() == 0)
                    this.abrePosicao(linha - 1, coluna - 1);
            }
            
            if (this.campo[linha - 1][coluna].getNumero() != 9 && !this.campo[linha - 1][coluna].isVisivel() && !this.campo[linha - 1][coluna].isBandeira()){
                this.campo[linha - 1][coluna].setVisivel(true);
                if (this.campo[linha - 1][coluna].getNumero() == 0)
                    this.abrePosicao(linha - 1, coluna);
            }
            
            if (colunaDepois && this.campo[linha - 1][coluna + 1].getNumero() != 9 && !this.campo[linha - 1][coluna + 1].isVisivel() && !this.campo[linha - 1][coluna + 1].isBandeira()){
                this.campo[linha - 1][coluna + 1].setVisivel(true);
                if (this.campo[linha - 1][coluna + 1].getNumero() == 0)
                    this.abrePosicao(linha - 1, coluna + 1);
            }
            
        }
        if (linhaDepois){
        	
            if (colunaAntes && this.campo[linha + 1][coluna - 1].getNumero() != 9 && !this.campo[linha + 1][coluna - 1].isVisivel() && !this.campo[linha + 1][coluna - 1].isBandeira()){
                this.campo[linha + 1][coluna - 1].setVisivel(true);
                if (this.campo[linha + 1][coluna - 1].getNumero() == 0)
                    this.abrePosicao(linha + 1, coluna - 1);
            }
            
            if (this.campo[linha + 1][coluna].getNumero() != 9 && !this.campo[linha + 1][coluna].isVisivel() && !this.campo[linha + 1][coluna].isBandeira()){
                this.campo[linha + 1][coluna].setVisivel(true);
                if (this.campo[linha + 1][coluna].getNumero() == 0)
                    this.abrePosicao(linha + 1, coluna);
            }
            
            if (colunaDepois && this.campo[linha + 1][coluna + 1].getNumero() != 9 && !this.campo[linha + 1][coluna + 1].isVisivel() && !this.campo[linha + 1][coluna + 1].isBandeira()){
                this.campo[linha + 1][coluna + 1].setVisivel(true);
                if (this.campo[linha + 1][coluna + 1].getNumero() == 0)
                    this.abrePosicao(linha + 1, coluna + 1);
            }
            
        }
	}
	
	public boolean jogada(int linha, int coluna){
        Casa casa = this.campo[linha][coluna];
        if (!casa.isVisivel()){
            casa.setVisivel(true);
            if (casa.getNumero() == 0)
                this.abrePosicao(linha, coluna);            	
            if (casa.getNumero() == 9)
                return false;
            return true;
        }        
        return false;
	}
	
	public boolean tem_jogada_disponivel(){
		for (int i = 0; i < this.dimensao; i++) {
			for (int j = 0; j < this.dimensao; j++) {
				if (!this.campo[i][j].isVisivel()) {
					return true;
				}
			}
		}
		return false;
	}

}