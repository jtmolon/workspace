package molon.joao.campominado_joaomolon;

import java.util.ArrayList;
import java.util.List;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.GridView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.os.Build;

public class MainActivity extends ActionBarActivity {
	GridView gridView;
	CampoMinado campo_minado = new CampoMinado(10, 10);
	Casa[][] campo = campo_minado.getCampo();
	List<Casa> casaList = new ArrayList<Casa>();
	Intent starterIntent;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		starterIntent = getIntent();
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		 
		for (int i = 0; i < campo_minado.getDimensao(); i++) {
			for (int j = 0; j < campo_minado.getDimensao(); j++){
				Casa casa = campo[i][j];            
				casaList.add(casa);        
			}
		}             
		gridView = (GridView) findViewById(R.id.gridView1);
		gridView.setAdapter(new CampoMinadoAdapter(this, casaList));
		gridView.setOnItemClickListener(new OnItemClickListener(){
        	@SuppressLint("NewApi")
			public void onItemClick(AdapterView<?> parent, View v, int position, long id){        		
        		Casa casaClicada = (Casa) gridView.getItemAtPosition(position);
        		if (!casaClicada.isVisivel() && !casaClicada.isBandeira()) {
        			campo_minado.jogada(casaClicada.getLinha(), casaClicada.getColuna());
            		onStart();
            		if (casaClicada.getNumero() == 9){
            			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            			builder.setTitle("Fim de Jogo");
            			builder.setMessage("Você perdeu");
            			builder.setNeutralButton("OK", null);
            			builder.show();
            			//restartar o jogo
            			if (android.os.Build.VERSION.SDK_INT >= 11)
            	        {
            	            recreate();
            	        }
            	        else
            	        {
            	        	startActivity(starterIntent);
            	        	finish();
            	        }
            		}
				}        		
        	}    
        });
		gridView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
				Casa casaClicada = (Casa) gridView.getItemAtPosition(position);
        		if (!casaClicada.isVisivel()){
       				casaClicada.setVisivel(true);
       				casaClicada.setBandeira(true);       				
        		}
        		else if (casaClicada.isBandeira()){
        			casaClicada.setVisivel(false);
        			casaClicada.setBandeira(false);
        		}        		        			
        		onStart();
				return true;
			}
		});
		
		
	}
	
	public void onStart(){
		super.onStart();
		gridView = (GridView) findViewById(R.id.gridView1);
		gridView.setAdapter(new CampoMinadoAdapter(this, casaList));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

}
