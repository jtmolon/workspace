package molon.joao.campominado_joaomolon;

public class Casa {

	private int numero;
	private boolean visivel;
	private boolean bandeira;
	private int linha;
	private int coluna;
		
	public Casa(int numero, boolean visivel, int linha, int coluna) {
		this.numero = numero;
		this.visivel = visivel;
		this.bandeira = false;
		this.linha = linha;
		this.coluna = coluna;
	}

	public int getLinha() {
		return linha;
	}

	public void setLinha(int linha) {
		this.linha = linha;
	}

	public int getColuna() {
		return coluna;
	}

	public void setColuna(int coluna) {
		this.coluna = coluna;
	}

	public int getNumero() {
		return numero;
	}

	public int getImagem() {
		int imagem;
		if (this.visivel){
			if (this.bandeira){
				imagem = R.drawable.bandeira;
			}
			else {
				switch (this.getNumero()) {
				case 0:	imagem = R.drawable.imagem_00;			
				break;
				case 1:	imagem = R.drawable.imagem_01;			
				break;
				case 2:	imagem = R.drawable.imagem_02;			
				break;
				case 3:	imagem = R.drawable.imagem_03;			
				break;
				case 4:	imagem = R.drawable.imagem_04;			
				break;
				case 5:	imagem = R.drawable.imagem_05;			
				break;
				case 6:	imagem = R.drawable.imagem_06;			
				break;
				case 7:	imagem = R.drawable.imagem_07;			
				break;
				case 8:	imagem = R.drawable.imagem_08;			
				break;
				case 9:	imagem = R.drawable.imagem_09;			
				break;		
				default:
					imagem = R.drawable.x;
					break;
				}
			}
		}
		else {
			imagem = R.drawable.x;
		}
		return imagem;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public boolean isVisivel() {
		return visivel;
	}

	public void setVisivel(boolean visivel) {
		this.visivel = visivel;
	}

	public boolean isBandeira() {
		return bandeira;
	}

	public void setBandeira(boolean bandeira) {
		this.bandeira = bandeira;
	}

	
}
