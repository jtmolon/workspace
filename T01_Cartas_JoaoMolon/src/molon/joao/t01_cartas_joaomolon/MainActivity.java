package molon.joao.t01_cartas_joaomolon;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;

public class MainActivity extends ActionBarActivity {
	EditText txtValorSelo, txtValorEnvelope, txtQtdeSelos, txtQtdeEnvelopes, txtDinheiro;
	Button btnCalculo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		txtValorSelo = (EditText) findViewById(R.id.txtValorSelo);
		txtValorEnvelope = (EditText) findViewById(R.id.txtValorEnvelope);
		txtQtdeSelos = (EditText) findViewById(R.id.txtQtdeSelos);
		txtQtdeEnvelopes = (EditText) findViewById(R.id.txtQtdeEnvelopes);
		txtDinheiro = (EditText) findViewById(R.id.txtDinheiro);
		btnCalculo = (Button) findViewById(R.id.btnCalculo);
		
		btnCalculo.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				
				String valorSelo = txtValorSelo.getText().toString();
				String valorEnvelope = txtValorEnvelope.getText().toString();
				String qtdeSelos = txtQtdeSelos.getText().toString();
				String qtdeEnvelopes = txtQtdeEnvelopes.getText().toString();
				String dinheiro = txtDinheiro.getText().toString();
				if (valorSelo.length() == 0 || valorEnvelope.length() == 0) {
					Toast.makeText(getApplicationContext(), 
							       "Informe o valor do selo e o valor do envelope", Toast.LENGTH_SHORT).show();
					return;					
				}
				else {
					Intent intencao = new Intent(getApplicationContext(), Resultados.class);
    				Bundle extras = new Bundle();
    				extras.putString("valorSelo",valorSelo);
    				extras.putString("valorEnvelope",valorEnvelope);
    				extras.putString("qtdeSelos",qtdeSelos);
    				extras.putString("qtdeEnvelopes",qtdeEnvelopes);
    				extras.putString("dinheiro",dinheiro);
    				intencao.putExtras(extras);					
					startActivity(intencao);
					
				}				
			}
		});
		
				
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

}
