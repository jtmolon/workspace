package molon.joao.t01_cartas_joaomolon;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Resultados extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		int qtdeSelos=0, qtdeEnvelopes=0;
		float valorSelo=0, valorEnvelope=0, dinheiro=0;
		TextView txtCartas, txtSelosRestantes, txtEnvelopesRestantes, txtDinheiroRestante;
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_resultados);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		txtCartas = (TextView) findViewById(R.id.lblCartas);
		txtSelosRestantes = (TextView) findViewById(R.id.lblSelosRestantes);
		txtEnvelopesRestantes = (TextView) findViewById(R.id.lblEnvelopesRestantes);
		txtDinheiroRestante = (TextView) findViewById(R.id.lblDinheiroRestante);
		
		Bundle extras = getIntent().getExtras();
		if(extras!=null){
			qtdeEnvelopes = Integer.parseInt(extras.getString("qtdeEnvelopes"));
			qtdeSelos = Integer.parseInt(extras.getString("qtdeSelos"));
			valorSelo = Float.parseFloat(extras.getString("valorSelo"));
			valorEnvelope = Float.parseFloat(extras.getString("valorEnvelope"));
			dinheiro = Float.parseFloat(extras.getString("dinheiro"));
							
			int qtdeCartas = 0;
			
			if (qtdeSelos > qtdeEnvelopes) {				
				qtdeCartas = qtdeEnvelopes;				
				qtdeSelos = qtdeSelos - qtdeEnvelopes;
				qtdeEnvelopes = 0;
				while ((qtdeSelos > 0) && (dinheiro > valorEnvelope)) {
					dinheiro = (float) dinheiro - valorEnvelope;
					qtdeSelos = (int) qtdeSelos - 1;
					qtdeCartas = (int) qtdeCartas + 1;	
				}
			}
			else{
				qtdeCartas = qtdeSelos;
				qtdeEnvelopes = qtdeEnvelopes - qtdeSelos;
				qtdeSelos = 0;				
				while ((qtdeEnvelopes > 0) && (dinheiro > valorSelo)) {
					dinheiro = (float) dinheiro - valorSelo;
					qtdeEnvelopes = (int) qtdeEnvelopes - 1;
					qtdeCartas = (int) qtdeCartas + 1;						
				}				
			}
			Toast.makeText(getApplicationContext(), String.format("%.2f", dinheiro), Toast.LENGTH_LONG).show();
			int qtdeDio;
			qtdeDio = (int) (dinheiro / (valorSelo + valorEnvelope));
			Toast.makeText(getApplicationContext(), String.format("%d - %d", qtdeDio, qtdeCartas), Toast.LENGTH_LONG).show();
			
			dinheiro = (float) dinheiro - ((float) qtdeDio * ((float) valorSelo + (float) valorEnvelope));
			qtdeCartas = (int) qtdeCartas + qtdeDio;
			
			txtCartas.setText(Integer.toString(qtdeCartas));
			txtSelosRestantes.setText(Integer.toString(qtdeSelos));
			txtEnvelopesRestantes.setText(Integer.toString(qtdeEnvelopes));
			txtDinheiroRestante.setText(String.format("%.2f", dinheiro));
						
		}		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.resultados, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_resultados,
					container, false);
			return rootView;
		}
	}

}
