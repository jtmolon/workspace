package com.exemplos.aula03_exemplo02_listview;

import java.util.ArrayList;
import java.util.List;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;
import android.app.ListActivity;

public class MainActivity extends ListActivity {
	/** Called when the activity is first created. */    
	@Override    
	public void onCreate(Bundle savedInstanceState) {        
		super.onCreate(savedInstanceState);                
		List<Estados> estadosList = new ArrayList<Estados>();                
		for (int i = 0; i < estados.length; i++) {            
			Estados estado = new Estados();            
			estado.setEstado(estados[i][0]);            
			estado.setAbreviacao(estados[i][1]);            
			estado.setCapital(estados[i][2]);            
			estado.setArea(Float.parseFloat(estados[i][3]));            
			estado.setBanner(images[i]);                        
			estadosList.add(estado);        
		}                  
		setListAdapter(new EstadoAdapter(this, estadosList));
		
		ListView lv = getListView();  
    	lv.setTextFilterEnabled(true);  
    	lv.setOnItemClickListener(new OnItemClickListener(){    
    		public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
    			// When clicked, show a toast with the TextView text 
    			String estado = estados[position][0];
    			view.setSelected(true);
    			Toast.makeText(getApplicationContext(), estado,Toast.LENGTH_SHORT).show();
    		}
    	});
	}        
	private String[][] estados = new String[][]{           
			{"Acre", "AC", "Rio Branco", "152581.4"},           
			{"Alagoas", "AL", "Macei�", "27767.7"},           
			{"Amap�", "AP", "Macap�", "142814.6"},           
			{"Amazonas", "AM", "Manaus", "1570745.7"},           
			{"Bahia", "BA", "Salvador", "564692.7"},           
			{"Cear�", "CE", "Fortaleza", "148825.6"},           
			{"Distrito Federal", "DF", "Bras�lia", "5822.1"},           
			{"Esp�rito Santo", "ES", "Vit�ria", "46077.5"},           
			{"Goi�s", "GO", "Goi�nia", "340086.7"},           
			{"Maranh�o", "MA", "S�o Lu�s", "331983.3"},           
			{"Mato Grosso", "MT", "Cuiab�", "903357.9"},           
			{"Mato Grosso do Sul", "MS", "Campo Grande", "357125.0"},           
			{"Minas Gerais", "MG", "Belo Horizonte", "586528.3"},           
			{"Par�", "PA", "Bel�m", "1247689.5"},           
			{"Para�ba", "PB", "Jo�o Pessoa", "56439.8"},           
			{"Paran�", "PR", "Curitiba", "199314.9"},           
			{"Pernambuco", "PE", "Recife", "98311.6"},           
			{"Piau�", "PI", "Teresina", "251529.2"},           
			{"Rio de Janeiro", "RJ", "Rio de Janeiro", "43696.1"},           
			{"Rio Grande do Norte", "RN", "Natal", "52796.8"},           
			{"Rio Grande do Sul", "RS", "Porto Alegre", "281748.5"},           
			{"Rond�nia", "RO", "Porto Velho", "237576.2"},           
			{"Roraima", "RR", "Boa Vista", "224299.0"},           
			{"Santa Catarina", "SC", "Florian�polis", "95346.2"},           
			{"S�o Paulo", "SP", "S�o Paulo", "248209.4"},           
			{"Sergipe", "SE", "Aracaju", "21910.3"},           
			{"Tocantins", "TO", "Palmas", "277620.9"}       
		};        
	private int[] images = new int[]{            
			R.drawable.acre,            
			R.drawable.alagoas,            
			R.drawable.amapa,            
			R.drawable.amazonas,            
			R.drawable.bahia,            
			R.drawable.ceara,            
			R.drawable.distrito_federal,            
			R.drawable.espirito_santo,            
			R.drawable.goias,            
			R.drawable.maranhao,            
			R.drawable.mato_grosso,            
			R.drawable.mato_grosso_do_sul,            
			R.drawable.minas_gerais,            
			R.drawable.para,            
			R.drawable.paraiba,            
			R.drawable.parana,            
			R.drawable.pernambuco,            
			R.drawable.piaui,            
			R.drawable.rio_de_janeiro,            
			R.drawable.rio_grande_do_norte,            
			R.drawable.rio_grande_do_sul,            
			R.drawable.rondonia,            
			R.drawable.roraima,            
			R.drawable.santa_catarina,            
			R.drawable.sao_paulo,            
			R.drawable.sergipe,            
			R.drawable.tocantins    
			};
	}