package com.exemplos.aula03_exemplo02_listview;

public class Estados {
	private String estado;    
	private String abreviacao;    
	private String capital;    
	private float area;    
	private int bandeira;        
	
	public String getEstado() {        
		return estado;    
	}    
	public void setEstado(String estado) {        
		this.estado = estado;    
	}    
	public String getAbreviacao() {        
		return abreviacao;    
	}    
	public void setAbreviacao(String abreviacao) {        
		this.abreviacao = abreviacao;    
	}    
	public String getCapital() {        
		return capital;    
	}    
	public void setCapital(String capital) {        
		this.capital = capital;    
	}    
	public float getArea() {        
		return area;    
	}    
	public void setArea(float area) {        
		this.area = area;    
	}    
	public int getBandeira() {        
		return bandeira;    
	}    
	public void setBanner(int bandeira) {        
		this.bandeira = bandeira;    
	}
}

