package molon.joao.quizcopa_joaomolon;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class RespostasProvider extends ContentProvider {

	public static final String PROVIDER_NAME = "molon.joao.RespostasProvider";
	public static final Uri CONTENT_URI = Uri.parse("content://"+ PROVIDER_NAME + "/respostas");
	      
	public static final String KEY_ROWID = "_id";
	public static final String KEY_PONTUACAO = "pontuacao";
	
	private static final int RESPOSTAS = 1;
	private static final int RESPOSTA_ID = 2;   
	         
	private static final UriMatcher uriMatcher;
	static{
	   uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	   uriMatcher.addURI(PROVIDER_NAME, "respostas", RESPOSTAS);
	   uriMatcher.addURI(PROVIDER_NAME, "respostas/#", RESPOSTA_ID);      
	}

   //--- Para uso do banco de dados ---
   private SQLiteDatabase respostasDB;

   private static final String DATABASE_NAME = "RespostasBD";
   private static final String DATABASE_TABLE = "respostas";
   private static final int DATABASE_VERSION = 1;
   private static final String CRIA_DATABASE = "create table respostas " +
               "(_id integer primary key autoincrement, " +
               " pontuacao int not null);" ;
	   
   private static class DatabaseHelper extends SQLiteOpenHelper{
      DatabaseHelper(Context context) {
	     super(context, DATABASE_NAME, null, DATABASE_VERSION);
	  }

	  @Override
	  public void onCreate(SQLiteDatabase db){
	     db.execSQL(CRIA_DATABASE);
	  }

	  @Override
	  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
         db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
	     onCreate(db);
	  }
  }
   
	public RespostasProvider() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onCreate() {
		   Context context = getContext();
		   DatabaseHelper dbHelper = new DatabaseHelper(context);
		   respostasDB = dbHelper.getWritableDatabase();
		   return (respostasDB == null)? false:true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		   //recebe uma consulta do cliente e retorna um cursor
		   
		   SQLiteQueryBuilder sqlBuilder = new SQLiteQueryBuilder();
		   sqlBuilder.setTables(DATABASE_TABLE);
		           
		   if (uriMatcher.match(uri) == RESPOSTA_ID)
		   //---if getting a particular book---
		      sqlBuilder.appendWhere(KEY_ROWID + " = " + uri.getPathSegments().get(1));                
		           
		      if (sortOrder==null || sortOrder=="")
		             sortOrder = KEY_PONTUACAO;
		       
		      Cursor c = sqlBuilder.query(
		             respostasDB, 
		             projection, 
		             selection, 
		             selectionArgs, 
		             null, 
		             null, 
		             sortOrder);
		       
		      //---register to watch a content URI for changes---
		      c.setNotificationUri(getContext().getContentResolver(), uri);
		      return c;
	}

	@Override
	public String getType(Uri uri) {
		   switch (uriMatcher.match(uri)){
	       //---get all books---
	       case RESPOSTAS:
	          return "vnd.android.cursor.dir/vnd.molon.joao.QuizCopa.respostas";
	       //---get a particular book---
	       case RESPOSTA_ID:                
	          return "vnd.android.cursor.item/vnd.com.exemplos.QuizCopa.respostas";
	       default:
	          throw new IllegalArgumentException("Unsupported URI: " + uri);        
	       }
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		   //insere um novo registro usando um ContentProvider
		   long idLinha = respostasDB.insert(DATABASE_TABLE, "", values);
		   
		   if (idLinha > 0){
		         Uri _uri = ContentUris.withAppendedId(CONTENT_URI, idLinha);
		         getContext().getContentResolver().notifyChange(_uri, null);    
		         return _uri;                
		      }        
		      throw new SQLException("Falha na inserção da linha na uri: " + uri);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
