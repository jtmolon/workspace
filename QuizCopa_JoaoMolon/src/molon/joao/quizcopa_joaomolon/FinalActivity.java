package molon.joao.quizcopa_joaomolon;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.os.Build;

public class FinalActivity extends Activity {
	private TextView lblPontuacao;
	private Button btnVoltar;
	private int pontuacao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_final);
		
		lblPontuacao = (TextView) findViewById(R.id.lblPontuacao);
		btnVoltar = (Button) findViewById(R.id.btnVoltar);
		
		Bundle extras = getIntent().getExtras();
		pontuacao = extras.getInt("pontuacao");
		
		lblPontuacao.setText(pontuacao + " pontos");
		
		//////////////////// Grava pontuação da jogada ////////////////////////////
        AsyncTask<Object, Object, Object> salvaPontuacaoTask = new AsyncTask<Object, Object, Object>(){
            @Override
            protected Object doInBackground(Object... params){
               // Salva o livro na base de dados
            	try{
   		         	ContentValues dados = new ContentValues();
   		         	ContentResolver cr = getContentResolver();
   		         	dados.put(RespostasProvider.KEY_PONTUACAO, pontuacao);
   		         	Uri uri = cr.insert(RespostasProvider.CONTENT_URI, dados);
            	}catch(SQLException e){
   		    	 	e.printStackTrace();
   		        }            	
               return null;
            } // end method doInBackground

            @Override
            protected void onPostExecute(Object result){
               super.onPostExecute(result);
            } 
        };
         
        // Salva o livro no BD usando uma thread separada
        salvaPontuacaoTask.execute((Object[]) null);		
        ///////////////////////////////////////////////////////////////////////////		
		
		btnVoltar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				Intent viewMain = new Intent(getApplicationContext(), MainActivity.class);
				startActivity(viewMain);
				finish();
			}
		});
		//if (savedInstanceState == null) {
		//	getSupportFragmentManager().beginTransaction()
		//			.add(R.id.container, new PlaceholderFragment()).commit();
		//}		
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.final_menu, menu);
        return true;
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_final,
					container, false);
			return rootView;
		}
	}

}
