package molon.joao.quizcopa_joaomolon;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.SimpleCursorAdapter;
import android.os.Build;

public class ListaRespostas extends ListActivity {
	private CursorAdapter respostasAdapter;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		String[] origem = new String[]{"pontuacao"};
		int[] destino = new int[] {R.id.txtPontuacao};
		int flags = 0;
		      
		respostasAdapter = new SimpleCursorAdapter(ListaRespostas.this,R.layout.list_resultados,null,origem,destino,flags);
		setListAdapter(respostasAdapter); // set contactView's adapter		
		//if (savedInstanceState == null) {
		//	getSupportFragmentManager().beginTransaction()
		//			.add(R.id.container, new PlaceholderFragment()).commit();
		//}
	}

	
	protected void onResume(){
	//sempre que executar onResume, ir� fazer uma busca no banco de dados
	//e vai atualizar a tela de exibi��o dos livros cadastrados
	   super.onResume(); 
	   new ObtemRespostas().execute((Object[]) null);
	} 
////////////////////////////////////////////////////////////
	// Quando precisamos dos resultados de uma opera��o do BD na thread da 
	// interface gr�fica, vamos usar AsyncTask para efetuar a opera��o em
	// uma thread e receber os resultados na thread da interface gr�fica
	private class ObtemRespostas extends AsyncTask<Object, Object, Cursor>{
	      @Override
	      protected Cursor doInBackground(Object... params){
	    	 Uri respostas = Uri.parse("content://"+ RespostasProvider.PROVIDER_NAME + "/respostas");
	 	     ContentResolver cr = getContentResolver();
	 	     Cursor cursor = cr.query(respostas, null, null, null, "pontuacao desc");
	         return cursor; //retorna todos os livros 
	      } // end method doInBackground

	      // use the Cursor returned from the doInBackground method
	      @Override
	      protected void onPostExecute(Cursor result){
	    	  respostasAdapter.changeCursor(result); //altera o cursor para um novo cursor
	      } // end method onPostExecute
	   } // end class GetContactsTask	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lista_respostas, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_lista_respostas,
					container, false);
			return rootView;
		}
	}

}
