package molon.joao.quizcopa_joaomolon;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.os.Build;

public class RespostaActivity extends Activity {
	private long idLinha;
	private TextView lblNivel;
	private TextView lblQuestao;
	private RadioButton rbAlt1;
	private RadioButton rbAlt2;
	private RadioButton rbAlt3;
	private RadioGroup rgAlternativas;
	private int altCorreta;
	private int nivelQuestao;
	private Button btnResponder;
	private CarregaQuestoesTask questoesTask;
	private Cursor questoesCursor;
	private int pontuacao = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_resposta);

		lblNivel = (TextView) findViewById(R.id.lblNivel);
		lblQuestao = (TextView) findViewById(R.id.lblQuestao);
		rbAlt1 = (RadioButton) findViewById(R.id.rbAlt1);
		rbAlt2 = (RadioButton) findViewById(R.id.rbAlt2);
		rbAlt3 = (RadioButton) findViewById(R.id.rbAlt3);
		rgAlternativas = (RadioGroup) findViewById(R.id.rgAlternativas);
		btnResponder = (Button) findViewById(R.id.btnRespoder);
		questoesTask = new CarregaQuestoesTask();
		questoesTask.execute();
		
		btnResponder.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String resposta = "Resposta errada! =(";
				if ((altCorreta == 1 && rbAlt1.isChecked()) ||
					(altCorreta == 2 && rbAlt2.isChecked()) ||
					(altCorreta == 3 && rbAlt3.isChecked())){
					pontuacao += nivelQuestao;
					resposta = "Resposta certa! =)";
				}
				AlertDialog.Builder builder = new AlertDialog.Builder(RespostaActivity.this);
    			builder.setTitle("");
    			builder.setMessage(resposta);
    			builder.setNeutralButton("OK", null);
    			builder.show();
				onStart();
			}
		});
		//if (savedInstanceState == null) {
		//	getSupportFragmentManager().beginTransaction()
		//			.add(R.id.container, new PlaceholderFragment()).commit();
		//}
	}

	   // Executa a consulta em uma thead separada
	   private class CarregaQuestoesTask extends AsyncTask<Long, Object, Cursor>{
	      @Override
	      protected Cursor doInBackground(Long... params){
	    	  Uri questoes = Uri.parse("content://molon.joao.QuestoesProvider/questoes");//params[0]
		 	  ContentResolver cr = getContentResolver();
		 	  Cursor cursor = cr.query(questoes, null, null, null, null);
		 	  //TODO: FAZER RANDOM DE 5 QUESTÕES
		      return cursor; //retorna as questões 
	      } 
	      // Usa o Cursor retornado do m�todo doInBackground
	      @Override
	      protected void onPostExecute(Cursor result) {
	         super.onPostExecute(result);
	         result.moveToFirst(); 
	         
	         int questaoIndex = result.getColumnIndex("questao");
	         int alt1Index = result.getColumnIndex("alt1");
	         int alt2Index = result.getColumnIndex("alt2");
	         int alt3Index = result.getColumnIndex("alt3");
	         int altcorretaIndex = result.getColumnIndex("altcorreta");	         
	         int nivelIndex = result.getColumnIndex("nivel");

	         lblQuestao.setText(result.getString(questaoIndex));
	         rbAlt1.setText(result.getString(alt1Index));
	         rbAlt2.setText(result.getString(alt2Index));
	         rbAlt3.setText(result.getString(alt3Index));
	         altCorreta = result.getInt(altcorretaIndex);
	         nivelQuestao = result.getInt(nivelIndex);
	         lblNivel.setText("Nível da questão: " + result.getString(nivelIndex));
	         //result.close(); 
	         questoesCursor  = result;
	      } 
	   } 	

	public void onStart(){   
		super.onStart();
		if (questoesCursor != null) {			
			if (questoesCursor.moveToNext()){
				int questaoIndex = questoesCursor.getColumnIndex("questao");
				int alt1Index = questoesCursor.getColumnIndex("alt1");
				int alt2Index = questoesCursor.getColumnIndex("alt2");
				int alt3Index = questoesCursor.getColumnIndex("alt3");
				int altcorretaIndex = questoesCursor.getColumnIndex("altcorreta");	         
				int nivelIndex = questoesCursor.getColumnIndex("nivel");
				
				lblQuestao.setText(questoesCursor.getString(questaoIndex));
				rbAlt1.setText(questoesCursor.getString(alt1Index));
				rbAlt1.setChecked(false);
				rbAlt2.setText(questoesCursor.getString(alt2Index));
				rbAlt2.setChecked(false);
				rbAlt3.setText(questoesCursor.getString(alt3Index));
				rbAlt3.setChecked(false);
				rgAlternativas.clearCheck();
				altCorreta = questoesCursor.getInt(altcorretaIndex);
		        nivelQuestao = questoesCursor.getInt(nivelIndex);
		        lblNivel.setText("Nível da questão: " + questoesCursor.getString(nivelIndex));
				//questoesCursor.close();
			}
			else{			
				questoesCursor.close();  			
    			Intent viewFinal = new Intent(RespostaActivity.this, FinalActivity.class);
    	        viewFinal.putExtra("pontuacao", pontuacao);
    	        startActivity(viewFinal);
    	        finish();
			}
		}	
	}
	   
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.resposta, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_resposta,
					container, false);
			return rootView;
		}
	}

}
