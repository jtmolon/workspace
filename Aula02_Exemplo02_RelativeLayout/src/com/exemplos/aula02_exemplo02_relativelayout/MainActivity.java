package com.exemplos.aula02_exemplo02_relativelayout;

import android.os.Bundle;
import android.app.Activity;
import android.content.res.Configuration;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	String TAG = "Orientacao";
	Button btn1, btn2, btn3, btn4, btn5, btn6, btn7;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        btn1 = (Button) findViewById(R.id.button1);
        btn2 = (Button) findViewById(R.id.button2);
        btn3 = (Button) findViewById(R.id.button3);
        btn4 = (Button) findViewById(R.id.button4);
        btn5 = (Button) findViewById(R.id.button5);
        btn6 = (Button) findViewById(R.id.button6);
        btn7 = (Button) findViewById(R.id.button7);
        
        btn1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(),"Bot�o 1", Toast.LENGTH_LONG).show();
			}
		});
        btn2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(),"Bot�o 2", Toast.LENGTH_LONG).show();
			}
		});
        btn3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(),"Bot�o 3", Toast.LENGTH_LONG).show();
			}
		});
        btn4.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(),"Bot�o 4", Toast.LENGTH_LONG).show();
			}
		});
        btn5.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(),"Bot�o 5", Toast.LENGTH_LONG).show();
			}
		});

        
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
        	
        	Log.d(TAG, "Landscape");
        	btn6.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					Toast.makeText(getApplicationContext(),"Bot�o 6", Toast.LENGTH_LONG).show();
				}
			});
        	btn7.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					Toast.makeText(getApplicationContext(),"Bot�o 7", Toast.LENGTH_LONG).show();
				}
			});	
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}
