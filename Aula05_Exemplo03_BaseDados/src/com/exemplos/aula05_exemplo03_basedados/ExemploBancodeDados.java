package com.exemplos.aula05_exemplo03_basedados;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.Toast;

public class ExemploBancodeDados extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	long id;
    	Cursor cursor;
    	boolean alterou;
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        DBAdapter db = new DBAdapter(this); 
        
//********************************
//--- adiciona departamentos---
        db.open();        
        id = db.insereDepto(1,"Financeiro");
        id = db.insereDepto(2,"Vendas");
        id = db.insereDepto(3,"Inform�tica");
        db.close();
        
//********************************
//--- adiciona funcionarios---
        db.open();        
        id = db.insereEmpregado("Rafael", 42, 2);
        id = db.insereEmpregado("Carina", 32, 3);
        id = db.insereEmpregado("Paula", 32, 2);
        id = db.insereEmpregado("Ricardo", 45, 1);
        db.close();    
//********************************
//--- obt�m todos os departamentos ---
        db.open();
        cursor = db.getTodosDeptos();
        if (cursor.moveToFirst() == true){
            do{          
                mostraDepto(cursor);
            }while (cursor.moveToNext());
        }
        db.close();
//********************************
//--- obt�m todos os empregados ---
        db.open();
        cursor = db.getTodosFuncionarios();
        if (cursor.moveToFirst() == true){
           do{          
              mostraFuncionario(cursor);
           }while (cursor.moveToNext());
        }
        db.close();
//*******************************************************************
//--- obt�m um funcionario ---
        db.open();
        cursor = db.getFuncionario(2); //objeto c declarado 
        if (cursor.moveToFirst())        
           mostraFuncionario(cursor);
        else
            Toast.makeText(this, "Funcion�rio nao encontrado!", Toast.LENGTH_LONG).show();
        db.close();      
    }
    
    public void mostraDepto(Cursor cursor){
    	String depto = "Id: " + cursor.getString(0) + "\n";
    	depto = depto + "Departamento: " + cursor.getString(1) + "\n";
    	
        Toast.makeText(this,depto,Toast.LENGTH_LONG).show();        
    } 
    public void mostraFuncionario(Cursor cursor){
    	String func = "Id: " + cursor.getString(0) + "\n";
    	func = func + "Nome: " + cursor.getString(1) + "\n";
    	func = func + "Idade: " + cursor.getString(2) + "\n";
    	func = func + "Departamento: " + cursor.getString(3) + "\n";
        Toast.makeText(this,func,Toast.LENGTH_LONG).show();        
    } 
}