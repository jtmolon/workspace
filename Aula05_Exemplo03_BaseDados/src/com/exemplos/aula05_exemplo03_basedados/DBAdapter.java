package com.exemplos.aula05_exemplo03_basedados;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBAdapter {
	 
	private static final String DATABASE_NAME = "Exemplo02BD";
	
		 static final String TABELA_EMPREGADOS="Empregados";
		 static final String KEY_ID="empID";
		 static final String KEY_NOME="nome";
		 static final String KEY_IDADE="idade";
		 static final String KEY_DEPTO="departamento";

		 static final String TABELA_DEPTO="Departamento";
		 static final String KEY_DEPTO_ID="deptoID";
		 static final String KEY_DEPTO_NOME="departamento";

		 static final String VIEW = "ViewEmps";
	 
		 private static final int DATABASE_VERSION =1;
	 
	 
		 static final String CRIA_TAB_DEPTO = "CREATE TABLE " + TABELA_DEPTO + 
				                              " (" + KEY_DEPTO_ID + " INTEGER PRIMARY KEY, " +
		                                             KEY_DEPTO_NOME + " TEXT);";
	 
		 static final String CRIA_TAB_EMP =  "CREATE TABLE " + TABELA_EMPREGADOS +
 		                                     " (" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,  " +
				                             KEY_NOME + " TEXT, " + KEY_IDADE + " INTEGER, " +
 		                                     KEY_DEPTO + " INTEGER NOT NULL ,FOREIGN KEY (" +
				                             KEY_DEPTO + ") REFERENCES " + 
 		                                     TABELA_DEPTO +" (" + KEY_DEPTO_ID + "));";

		 static final String CRIA_TRIGGER = "CREATE TRIGGER fk_empdept_deptid " +
  			                                " BEFORE INSERT ON " + TABELA_EMPREGADOS +
  			                                " FOR EACH ROW BEGIN "+
  			                                " SELECT CASE WHEN ((SELECT " + KEY_DEPTO_ID + 
  			                                " FROM " + TABELA_DEPTO + 
  			                                " WHERE "+ KEY_DEPTO_ID + "=new."+ KEY_DEPTO +
  			                                " ) IS NULL) THEN RAISE (ABORT,'Foreign Key Violation') END;" +
  			                                "  END;";
		 
		 static final String CRIA_VIEW = "CREATE VIEW "+ VIEW +
				                         " AS SELECT " + TABELA_EMPREGADOS + "." + KEY_ID + " AS _id," +
 			                              TABELA_EMPREGADOS + "." + KEY_NOME + ", " + 
		                                  TABELA_EMPREGADOS + "."+ KEY_IDADE + ", " +
 			                              TABELA_DEPTO + "."+ KEY_DEPTO_NOME + 
 			                              " FROM " + TABELA_EMPREGADOS + 
 			                              " JOIN " + TABELA_DEPTO +
 			                              " ON "+ TABELA_EMPREGADOS + "." + KEY_DEPTO + 
 			                              " = " + TABELA_DEPTO + "." + KEY_DEPTO_ID;
		 
 			 
		 
	 private final Context context;  
	    private DatabaseHelper DBHelper;
	    private SQLiteDatabase db;

	    public DBAdapter(Context ctx){
	        this.context = ctx;
	        DBHelper = new DatabaseHelper(context); //classe interna que herda de SQLiteOpenHelper
	    }
	   
	    //classe interna que manipula o banco
	    //SQLiteOpenHelper � uma classe abstrata. 
	    private static class DatabaseHelper extends SQLiteOpenHelper{
	        DatabaseHelper(Context context){
	            super(context, DATABASE_NAME, null, DATABASE_VERSION);
	        }
	        @Override
	        public void onOpen(SQLiteDatabase db) {//for�a integridade referencial
	        		super.onOpen(db);
	        		if (!db.isReadOnly()) {
	        			// Enable foreign key constraints
	        			db.execSQL("PRAGMA foreign_keys=ON;");
	        		}
	        }
	        @Override
	        public void onCreate(SQLiteDatabase db){
	        	try{
	        		
	        		db.execSQL(CRIA_TAB_DEPTO);
	        		db.execSQL(CRIA_TAB_EMP);
	        		db.execSQL(CRIA_TRIGGER);	  
	        		db.execSQL(CRIA_VIEW);  	        		
	        	}
	        	catch(SQLException e){
	        		e.printStackTrace();
	        	}
	        }

	        @Override
	        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	        	  db.execSQL("DROP TABLE IF EXISTS "+TABELA_EMPREGADOS);
	        	  db.execSQL("DROP TABLE IF EXISTS "+TABELA_DEPTO);
	        	  
	        	  db.execSQL("DROP TRIGGER IF EXISTS dept_id_trigger");
	        	  db.execSQL("DROP TRIGGER IF EXISTS dept_id_trigger22");
	        	  db.execSQL("DROP TRIGGER IF EXISTS fk_empdept_deptid");
	        	  db.execSQL("DROP VIEW IF EXISTS "+ VIEW);
	        	  onCreate(db);
	        	 }
	    }    
	    
// *******************************************************************************
	    //--- abre a base de dados ---
	    public DBAdapter open() throws SQLException{
	        db = DBHelper.getWritableDatabase();
	        return this;
	    }

	    //--- fecha a base de dados ---    
	    public void close(){
	        DBHelper.close();
	    }
	    
	    //---insere um Livro na base da dados ---
	    public long insereDepto(int codDepto, String nome){
	    	  ContentValues cv=new ContentValues();
	    	   cv.put(KEY_DEPTO_ID, codDepto);
	    	   cv.put(KEY_DEPTO_NOME, nome);
	    	   return db.insert(TABELA_DEPTO, KEY_DEPTO_ID, cv);
	    }
	    
	    public long insereEmpregado(String nome, int idade, int departamento){
	    	  ContentValues cv=new ContentValues();
	    	   cv.put(KEY_NOME, nome);
	    	   cv.put(KEY_IDADE, idade);
	    	   cv.put(KEY_DEPTO, departamento);
	    	   return db.insert(TABELA_EMPREGADOS, null, cv);
	    }

	    //--- devolve todos os departamentos ---
	    public Cursor getTodosDeptos(){
	       	   String [] colunas =new String[]{KEY_DEPTO_ID, KEY_DEPTO_NOME};
	       	   Cursor c = db.query(TABELA_DEPTO, colunas, null, null, null, null, null);
	    	   return c;
	    }
	    //--- devolve todos os funcionarios---
	    public Cursor getTodosFuncionarios(){
	       	   String [] colunas =new String[]{"_id",KEY_NOME, KEY_IDADE,KEY_DEPTO_NOME};
	       	   Cursor c = db.query(VIEW, colunas, null, null, null, null, null);
	    	   return c;
	    }

	    //--- recupera um funcionarios ---
	    public Cursor getFuncionario(long idLinha) throws SQLException{
	        
	    	String [] colunas =new String[]{"_id",KEY_NOME, KEY_IDADE,KEY_DEPTO_NOME};
	    	String linhaAcessada = "_id" + "=" + idLinha;
	        Cursor mCursor = db.query(VIEW, colunas,linhaAcessada,null,null,null,null,null); 

	        if (mCursor != null) {
	            mCursor.moveToFirst();
	        }
	        return mCursor;
	    }
}