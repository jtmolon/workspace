package com.exemplos.aula10_exemplo04_fragment;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

//faz a extens�o da classe FragmentActivity...
public class MainActivity extends FragmentActivity implements OnPictureItemClickListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// se tiver o container de fragments � um smartphone
		if (findViewById(R.id.fragment_container) != null) {

			if (savedInstanceState != null)
				return;

			Lista_Imagens_Fragment listPictureFragment = new Lista_Imagens_Fragment();
			FragmentManager fm = getFragmentManager();
			getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, listPictureFragment).commit();
		}

	}

	//Implementa��o do m�todo da Interface OnPictureItemClickListener
	public void onPictureItemClicked(int position) {

		Imagem_Fragment pictureViewFragment = (Imagem_Fragment) getSupportFragmentManager().findFragmentById(R.id.PictureViewFragmentId);

		if (pictureViewFragment != null) {

			pictureViewFragment.mostraImagem(position);

		} else {
			
			Imagem_Fragment newFragment = new Imagem_Fragment();
			Bundle args = new Bundle();
			args.putInt(Imagem_Fragment.ARG_POSITION, position);
			newFragment.setArguments(args);
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.fragment_container, newFragment);
			transaction.addToBackStack(null);
			transaction.commit();		
			}
	}
}