package com.exemplos.aula10_exemplo04_fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class Imagem_Fragment extends Fragment {

	final static String ARG_POSITION = "position";
	int posAtual = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (savedInstanceState != null) {
			posAtual = savedInstanceState.getInt(ARG_POSITION);
		}

		return inflater.inflate(R.layout.imagem, container, false);
	}

	@Override
	public void onStart() {

		super.onStart();

		Bundle args = getArguments();
		if (args != null) {
					mostraImagem(args.getInt(ARG_POSITION));
		} else if (posAtual != -1) {
		
			mostraImagem(posAtual);
		}
	}

	public void mostraImagem(int position) {
		ImageView imageView = (ImageView) getActivity().findViewById(R.id.picture_view_fragment_imageId);
		imageView.setImageResource(Lista_Imagens.referencias[position]);
		posAtual = position;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(ARG_POSITION, posAtual);
	}
}