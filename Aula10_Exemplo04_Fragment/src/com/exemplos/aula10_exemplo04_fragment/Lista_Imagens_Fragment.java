package com.exemplos.aula10_exemplo04_fragment;

import android.support.v4.app.ListFragment;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Lista_Imagens_Fragment extends ListFragment {

	OnPictureItemClickListener privateCallback;


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		privateCallback = (OnPictureItemClickListener) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

		int layout = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? android.R.layout.simple_list_item_activated_1 : android.R.layout.simple_list_item_1;

		setListAdapter(new ArrayAdapter<String>(getActivity(), layout, Lista_Imagens.nomes));

	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		 if (getFragmentManager().findFragmentById(R.id.ListPictureFragmentId) != null) {
	            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
	        }
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		privateCallback.onPictureItemClicked(position);
	    getListView().setItemChecked(position, true);
	}
}
