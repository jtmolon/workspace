package com.exemplos.aula10_exemplo04_fragment;

public interface OnPictureItemClickListener  {

	public void onPictureItemClicked(int position);
}