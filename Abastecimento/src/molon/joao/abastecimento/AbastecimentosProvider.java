package molon.joao.abastecimento;


import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class AbastecimentosProvider extends ContentProvider {
	public static final String PROVIDER_NAME = "molon.joao.abastecimentos";
	public static final Uri CONTENT_URI = Uri.parse("content://"+ PROVIDER_NAME + "/abastecimentos");
	      
	public static final String KEY_ROWID = "_id";
	public static final String KEY_DATA = "data";
	public static final String KEY_KM = "km";
	public static final String KEY_LITROS = "litros";
	      
	private static final int ABASTECIMENTOS = 1;
	private static final int ABASTECIMENTO_ID = 2;   
	         
	private static final UriMatcher uriMatcher;
	static{
	   uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	   uriMatcher.addURI(PROVIDER_NAME, "abastecimentos", ABASTECIMENTOS);
	   uriMatcher.addURI(PROVIDER_NAME, "abastecimentos/#", ABASTECIMENTO_ID);      
	}

   //--- Para uso do banco de dados ---
   private SQLiteDatabase abastecimentosDB;

   private static final String DATABASE_NAME = "AbastecimentosBD";
   private static final String DATABASE_TABLE = "abastecimentos";
   private static final int DATABASE_VERSION = 1;
   private static final String CRIA_DATABASE = "create table abastecimentos " +
               "(_id integer primary key autoincrement, " +
               " data string not null," +
               " km integer not null," +
               " litros real not null);" ;

   private static class DatabaseHelper extends SQLiteOpenHelper{
	      DatabaseHelper(Context context) {
		     super(context, DATABASE_NAME, null, DATABASE_VERSION);
		  }

		  @Override
		  public void onCreate(SQLiteDatabase db){
		     db.execSQL(CRIA_DATABASE);
		  }

		  @Override
		  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	         db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
		     onCreate(db);
		  }
   }
	     
   
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		   switch (uriMatcher.match(uri)){
	       //---get all books---
	       case ABASTECIMENTOS:
	          return "vnd.android.cursor.dir/vnd.molon.joao.abastecimentos.abastecimentos";
	       //---get a particular book---
	       case ABASTECIMENTO_ID:                
	          return "vnd.android.cursor.item/vnd.molon.joao.abastecimentos.abastecimentos";
	       default:
	          throw new IllegalArgumentException("Unsupported URI: " + uri);        
	       }
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		long idLinha = abastecimentosDB.insert(DATABASE_TABLE, "", values);
		   
		if (idLinha > 0){
		     Uri _uri = ContentUris.withAppendedId(CONTENT_URI, idLinha);
		     getContext().getContentResolver().notifyChange(_uri, null);    
		     return _uri;                
		}        
		throw new SQLException("Falha na inser��o da linha na uri: " + uri);
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		Context context = getContext();
		DatabaseHelper dbHelper = new DatabaseHelper(context);
		abastecimentosDB = dbHelper.getWritableDatabase();
		return (abastecimentosDB == null)? false:true;		
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		   SQLiteQueryBuilder sqlBuilder = new SQLiteQueryBuilder();
		   sqlBuilder.setTables(DATABASE_TABLE);
		           
		   if (uriMatcher.match(uri) == ABASTECIMENTO_ID)
		   //---if getting a particular book---
		      sqlBuilder.appendWhere(KEY_ROWID + " = " + uri.getPathSegments().get(1));                
		           
		   if (sortOrder==null || sortOrder=="")
		       sortOrder = KEY_DATA;
		       
		   Cursor c = sqlBuilder.query(
		             abastecimentosDB, 
		             projection, 
		             selection, 
		             selectionArgs, 
		             null, 
		             null, 
		             sortOrder);
		       
		   //---register to watch a content URI for changes---
		   c.setNotificationUri(getContext().getContentResolver(), uri);
		   return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
