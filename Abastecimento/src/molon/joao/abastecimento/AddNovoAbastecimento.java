package molon.joao.abastecimento;

import com.exemplos.aula06_exemplo05_contentprovider.AddNovoLivro;
import com.exemplos.aula06_exemplo05_contentprovider.R;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class AddNovoAbastecimento extends Activity {
	private long idLinha;
	private EditText txtData;
	private EditText txtKm;
	private EditText txtLitros;
	private Button btnSalvar;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_novo_abastecimento);
		
		txtData = (EditText) findViewById(R.id.txtData);
		txtKm = (EditText) findViewById(R.id.txtKM);
		txtLitros = (EditText) findViewById(R.id.txtLitros);
		btnSalvar = (Button) findViewById(R.id.btnSalvar);
		
		Bundle extras = getIntent().getExtras(); // get Bundle of extras
		
	      // Se h� extras, usa os valores para preencher a tela
	      if (extras != null){
	         idLinha = extras.getLong("idLinha");
	         txtData.setText(extras.getString("data"));  
	         txtKm.setText(extras.getString("km"));  
	         txtLitros.setText(extras.getString("litros"));  
	      } // end if
	    
	      btnSalvar = (Button) findViewById(R.id.btnSalvar);
	      btnSalvar.setOnClickListener(salvarAbastecimentoButtonClicked);		
	}

	   OnClickListener salvarAbastecimentoButtonClicked = new OnClickListener(){
		      public void onClick(View v){
		         if (txtData.getText().length() != 0){
		            AsyncTask<Object, Object, Object> salvaLivroTask = new AsyncTask<Object, Object, Object>(){
		                  @Override
		                  protected Object doInBackground(Object... params){
		                     salvaLivro(); // Salva o livro na base de dados
		                     return null;
		                  } // end method doInBackground
		      
		                  @Override
		                  protected void onPostExecute(Object result){
		                     finish(); // Fecha a atividade
		                  } 
		               };
		               
		            // Salva o livro no BD usando uma thread separada
		            salvaLivroTask.execute((Object[]) null); 
		         } // end if
		         else {
		            // Cria uma caixa de di�logo 
		            AlertDialog.Builder builder = new AlertDialog.Builder(AddNovoAbastecimento.this);
		            builder.setTitle(R.string.tituloErro); 
		            builder.setMessage(R.string.mensagemErro);
		            builder.setPositiveButton(R.string.botaoErro, null); 
		            builder.show(); 
		         } 
		      } 
		   }; 	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_novo_abastecimento, menu);
		return true;
	}

}
