package com.examplos.aula01_exemplo02;

import android.os.Bundle;
import android.util.Log;
import android.app.Activity;

public class Activity2 extends Activity {
	String tag= "Eventos";
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
        Log.d(tag,"Estou no evento onCreate() da Activity2");
    }
    public void onStart(){
        super.onStart();
        Log.d(tag, "Estou no evento onStart() da Activity2");
    }    
    public void onRestart(){
        super.onRestart();
        Log.d(tag, "Estou no evento onRestart() da Activity2");
    }    
    public void onResume(){
        super.onResume();
        Log.d(tag, "Estou no evento onResume() da Activity2");
    }
    public void onPause(){
        super.onPause();
        Log.d(tag, "Estou no evento onPause() da Activity2");
    }    
    public void onStop(){
        super.onStop();
        Log.d(tag, "Estou no evento onStop() da Activity2");
    }    
    public void onDestroy(){
        super.onDestroy();
        Log.d(tag, "Estou no evento onDestroy() da Activity2");
    }   

}