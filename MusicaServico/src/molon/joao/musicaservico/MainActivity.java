package molon.joao.musicaservico;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import molon.joao.musicaservico.Servico;
import molon.joao.musicaservico.Servico.LocalBinder;
import android.os.Bundle;
import android.os.IBinder;
import android.app.Activity;
import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends ListActivity {
	Servico servico;

    ServiceConnection conexao = new ServiceConnection() {
		public void onServiceConnected(ComponentName name, IBinder srv) {
			LocalBinder binder = (LocalBinder) srv;
	        servico = binder.getService();
	        conectado = true;
	    }

		public void onServiceDisconnected(ComponentName name) {
			conectado = false;
		}
	};		
	
	boolean conectado = false;
	private static final String MEDIA_PATH = new String("/sdcard/musicas/");
	private List<String> musicas = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		atualizaListaMusicas();
		
		Intent intent = new Intent(this, Servico.class);
		bindService(intent, conexao, Context.BIND_AUTO_CREATE);
	}

	
    public void atualizaListaMusicas() {
    	File home = new File(MEDIA_PATH);
    	if (home.listFiles( new Mp3Filter()).length > 0) {
    		for (File file : home.listFiles( new Mp3Filter())) {
    			musicas.add(file.getName());
    		}
		
    		ArrayAdapter<String> songList = new ArrayAdapter<String>(this,R.layout.list_item,musicas);
    		setListAdapter(songList);
		}
    }
	@Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
		//chama o servi�o
		if (servico.getMusicas() == null) {
			servico.setMusicas(musicas);
		}		
		servico.tocaMusica(position);
	}	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onStart() {
	    super.onStart();
	} 

	@Override
	protected void onStop() {
	    super.onStop();
	    // Desconecta do servi�o
	    if (conectado) {
	        unbindService(conexao);
	        conectado = false;
	    }
	}	
	
	
}

class Mp3Filter implements FilenameFilter {
    public boolean accept(File dir, String name) {
        return (name.endsWith(".mp3"));
    }
}
