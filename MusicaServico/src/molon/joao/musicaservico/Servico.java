package molon.joao.musicaservico;

import java.io.IOException;
import java.util.List;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class Servico extends Service {
	private List<String> musicas;
	private final IBinder meuBinder = new LocalBinder();
	private MediaPlayer mp = new MediaPlayer();
	private static final String MEDIA_PATH = new String("/sdcard/musicas/");
	
    public class LocalBinder extends Binder {
        Servico getService() {
            return Servico.this;
        }
    }	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return meuBinder;
	}
	public List<String> getMusicas(){
		return this.musicas;
	}
    public void setMusicas(List<String> _musicas){
    	this.musicas= _musicas;
    }
	public void tocaMusica(int position) {
		try {
			mp.reset();
			String caminho = MEDIA_PATH + musicas.get(position);
			mp.setDataSource(caminho);
			mp.prepare();
			mp.start();
		} catch (IOException e) {
			Log.v(getString(R.string.app_name), e.getMessage());
		}
	}
}
