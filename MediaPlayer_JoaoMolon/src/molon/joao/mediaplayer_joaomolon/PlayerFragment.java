package molon.joao.mediaplayer_joaomolon;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.MediaController.MediaPlayerControl;
import android.widget.VideoView;

public class PlayerFragment extends Fragment implements MediaPlayerControl {
	final static String ARG_POSITION = "position";
	int posAtual = -1;
	private MusicController controller;
		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (savedInstanceState != null) {
			posAtual = savedInstanceState.getInt(ARG_POSITION);
		}
		
		return inflater.inflate(R.layout.player_fragment, container, false);
	}

	@Override
	public void onStart() {

		super.onStart();

		Bundle args = getArguments();
		if (args != null){
			if (!args.getBoolean("tocando")){
				tocaMusica(args.getInt(ARG_POSITION));
			} else {
				setController();
				setTocando();
			}
		} else if (posAtual != -1) {
			tocaMusica(posAtual);
		}
	}

	public void setTocando(){
		if (MainActivity.servico != null) {
			TextView lblMusica;
			lblMusica = (TextView) getActivity().findViewById(R.id.lblMusica);
			int position = MainActivity.servico.getPosAtual();
			lblMusica.setText(MainActivity.servico.getMusicas().get(position));	
		}		
	}

	public void tocaMusica(int position) {
		MainActivity.servico.setPosAtual(position);
		MainActivity.servico.tocaMusica();
		setTocando();
		
		if (MainActivity.playbackPaused) {
			
			MainActivity.playbackPaused = false;
		}
		setController();
		controller.show(0);
		//servico.tocaMusica(position);;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(ARG_POSITION, posAtual);
	}	
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		setController();
		setTocando();		
	}
	
	public void setController(){
		if (controller == null){
			controller = new MusicController(getActivity());
		}
		controller.setPrevNextListeners(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tocaProxima();
			}
			}, new View.OnClickListener() {
			  @Override
			  public void onClick(View v) {
			    tocaAnterior();
			  }
		});
		controller.setMediaPlayer(this);
		VideoView videoView;
		videoView = (VideoView)getActivity().findViewById(R.id.videoView1);
		controller.setAnchorView(videoView);
		controller.setEnabled(true);
	}	
	
	private void tocaProxima(){
		MainActivity.servico.tocaProxima();
		setTocando();
		if(MainActivity.playbackPaused){
		    setController();
		    MainActivity.playbackPaused=false;
		}		
		controller.show(0);
	}
	
	private void tocaAnterior(){
		MainActivity.servico.tocaAnterior();
		setTocando();
		if(MainActivity.playbackPaused){
		    setController();
		    MainActivity.playbackPaused=false;
		}		
		controller.show(0);
	}
	
	
	public PlayerFragment() {
		// TODO Auto-generated constructor stub		
	}

	@Override
	public void start() {
		MainActivity.servico.go();
	}

	@Override
	public void pause() {
		MainActivity.servico.pausePlayer();		
		MainActivity.playbackPaused = true;
	}

	@Override
	public int getDuration() {
		if(MainActivity.servico!=null && MainActivity.conectado && MainActivity.servico.isTocando())
		    return MainActivity.servico.getDuracao();
		  else return 0;
	}

	@Override
	public int getCurrentPosition() {
		if(MainActivity.servico!=null && MainActivity.conectado && MainActivity.servico.isTocando())
		    return MainActivity.servico.getPosicao();
		  else return 0;
	}

	@Override
	public void seekTo(int pos) {
		MainActivity.servico.seek(pos);
	}

	@Override
	public boolean isPlaying() {
		if(MainActivity.servico!=null && MainActivity.conectado)
		    return MainActivity.servico.isTocando();
		  return false;
	}

	@Override
	public int getBufferPercentage() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean canPause() {
		return true;
	}

	@Override
	public boolean canSeekBackward() {
		return true;
	}

	@Override
	public boolean canSeekForward() {
		return true;
	}

	@Override
	public int getAudioSessionId() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onPause(){
	  super.onPause();
	  MainActivity.paused=true;
	}

	@Override
	public void onResume(){
	  super.onResume();
	  if(MainActivity.paused){
	    setController();
	    MainActivity.paused=false;
	  }
	}
	
	@Override
	public void onStop() {
	  controller.hide();
	  super.onStop();
	}
	
	
}
