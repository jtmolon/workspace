package molon.joao.mediaplayer_joaomolon;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import molon.joao.mediaplayer_joaomolon.Servico;
import molon.joao.mediaplayer_joaomolon.Servico.LocalBinder;
import android.os.Bundle;
import android.os.IBinder;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends FragmentActivity implements OnMusicItemClickListener{
	public static Servico servico = null;
	public static final int NOTIFY_ID=1;
	public static boolean paused=false, playbackPaused=false;
	public static boolean conectado = false;
	private static final String MEDIA_PATH = new String("/sdcard/musicas/");
	private List<String> musicas = new ArrayList<String>();
    
	ServiceConnection conexao = new ServiceConnection() {
		public void onServiceConnected(ComponentName name, IBinder srv) {
			LocalBinder binder = (LocalBinder) srv;
	        if (servico == null){
	        	servico = binder.getService();
	        }
	        conectado = true;
	    }

		public void onServiceDisconnected(ComponentName name) {
			conectado = false;
		}
	};		
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Intent intent = new Intent(this, Servico.class);
		bindService(intent, conexao, Context.BIND_AUTO_CREATE);
		
		// se tiver o container de fragments � um smartphone
		if (findViewById(R.id.fragment_container) != null) {

			if (savedInstanceState != null) 
				return;
			
			FragmentManager fm = getFragmentManager();

			ListaMusicasFragment listMusicasFragment;
			listMusicasFragment = new ListaMusicasFragment();			
			FragmentTransaction transactionLista = getSupportFragmentManager().beginTransaction();
		    transactionLista.replace(R.id.fragment_container, listMusicasFragment);
		    transactionLista.addToBackStack(null);
		    transactionLista.commit();			
			
			if (servico != null && servico.isTocando()){
				
				PlayerFragment playerFragment = 
						(PlayerFragment) getSupportFragmentManager().findFragmentById(R.id.PlayerFragmentId);
				
				if (playerFragment != null) {

					//playerFragment.tocaMusica(position);
					

				} else {
					PlayerFragment newFragment = new PlayerFragment();
					Bundle args = new Bundle();
					args.putBoolean("tocando", true);
					newFragment.setArguments(args);
					FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
					transaction.replace(R.id.fragment_container, newFragment);
					transaction.addToBackStack(null);
					transaction.commit();
				}
				
			}
		}		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onStart() {
	    super.onStart();
	    
	    
	} 

	@Override
	protected void onResume() {
	    super.onResume();
	}	
	
	@Override
	protected void onStop(){
		super.onStop();
		
	}
	
	@Override
	protected void onDestroy() {
	    super.onDestroy();
	    // Desconecta do servi�o
	    if (conectado) {
	        unbindService(conexao);
	        conectado = false;
	    }
	}

	@Override
	protected void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);		
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if (getFragmentManager().getBackStackEntryCount() == 0) {
	        this.finish();
	    } else {
	        getFragmentManager().popBackStack();
	    }
	}
	
	 @Override
	 public boolean onKeyDown(int keyCode, KeyEvent event)
	 {
	 if (keyCode == KeyEvent.KEYCODE_BACK)
	 {
	    if (getSupportFragmentManager().getBackStackEntryCount() == 0)
	    {
	        this.finish();
	        return false;
	    }
	    else
	    {
	        getSupportFragmentManager().popBackStack();
	        //removeCurrentFragment();

	        return false;
	    }
	 }

	  return super.onKeyDown(keyCode, event);
	 }	

	@Override
	public void onMusicItemClicked(int position) {				
		
		PlayerFragment playerFragment = 
				(PlayerFragment) getSupportFragmentManager().findFragmentById(R.id.PlayerFragmentId);
		
		if (playerFragment != null) {

			playerFragment.tocaMusica(position);

		} else {
			
			PlayerFragment newFragment = new PlayerFragment();
			//newFragment.setController();
			Bundle args = new Bundle();
			args.putInt(PlayerFragment.ARG_POSITION, position);
			args.putBoolean("tocando", false);
			newFragment.setArguments(args);
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.fragment_container, newFragment);
			transaction.addToBackStack(null);
			transaction.commit();		
			}		
	}	
	

	
}

class Mp3Filter implements FilenameFilter {
    public boolean accept(File dir, String name) {
        return (name.endsWith(".mp3"));
    }
}
