package molon.joao.mediaplayer_joaomolon;

import java.io.IOException;
import java.util.List;

import molon.joao.mediaplayer_joaomolon.R;
import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.content.ContentUris;
import android.database.Cursor;
import android.database.SQLException;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.PowerManager;
import android.util.Log;
import android.widget.TextView;

import java.util.Random;

import android.app.Notification;
import android.app.PendingIntent;

public class Servico extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
MediaPlayer.OnCompletionListener{
	private List<String> musicas;
	private final IBinder meuBinder = new LocalBinder();
	private MediaPlayer mp;
	private static final String MEDIA_PATH = new String("/sdcard/musicas/");
	private int posAtual;
	private int NOTIFY_ID = 1;
	
    public class LocalBinder extends Binder {
        Servico getService() {
            return Servico.this;
        }
    }	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return meuBinder;
	}
	
	public void onCreate(){
		super.onCreate();
		posAtual = 0;
		mp = new MediaPlayer();
		initMusicPlayer();
	}
	
	public void initMusicPlayer(){
		mp.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
		mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mp.setOnPreparedListener(this);
		mp.setOnCompletionListener(this);
		mp.setOnErrorListener(this);
	}
	
	
	public List<String> getMusicas(){
		return this.musicas;
	}
    public void setMusicas(List<String> _musicas){
    	this.musicas= _musicas;
    }
    
    public void setPosAtual(int pos){
    	this.posAtual = pos;
    }
    
    public int getPosAtual(){
    	return this.posAtual;
    }
    
	public void tocaMusica() {
		try {
			mp.reset();
			String caminho = MEDIA_PATH + musicas.get(this.posAtual);
			mp.setDataSource(caminho);
			mp.prepareAsync();
			//mp.start();
		} catch (IOException e) {
			Log.v(getString(R.string.app_name), e.getMessage());
		}
	}
	@Override
	public void onCompletion(MediaPlayer mp) {
		if(mp.getCurrentPosition() > 0){
		    mp.reset();
		    tocaProxima();
		}		
	}
	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		mp.reset();
		return false;
	}
	@Override
	public void onPrepared(MediaPlayer mp) {
		mp.start();
		registrarExecucao();
		
		Intent notIntent = new Intent(this, MainActivity.class);
		notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		notIntent.putExtra("tocando", true);
		PendingIntent pendInt = PendingIntent.getActivity(this, 0,
		  notIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		 
		Notification.Builder builder = new Notification.Builder(this);
		 
		builder.setContentIntent(pendInt)
		  .setSmallIcon(R.drawable.play)
		  .setTicker(musicas.get(posAtual))
		  .setOngoing(true)
		  .setContentTitle("Tocando")
		  .setContentText(musicas.get(posAtual));
		Notification not = builder.build();		
		 
		startForeground(NOTIFY_ID, not);		
	}
	
	//@Override
	//public boolean onUnbind(Intent intent){
	//  mp.stop();
	//  mp.release();
	//  return false;
	//}
	
	@Override
	public void onDestroy(){
		stopForeground(true);
	}
	
	public int getPosicao(){
		return mp.getCurrentPosition();
	}

	public int getDuracao(){
		return mp.getDuration();
	}

	public boolean isTocando(){
		return mp.isPlaying();
	}

	public void pausePlayer(){
		mp.pause();
	}

	public void seek(int posn){
		mp.seekTo(posn);
	}

	public void go(){
		mp.start();
	}	
	
	public void tocaAnterior(){
	  posAtual--;
	  if (posAtual < 0){ 
		  	posAtual=musicas.size()-1;
	  }
	  tocaMusica();
	}
	
	public void tocaProxima(){
	  posAtual++;
	  if(posAtual >= musicas.size()){ 
		  posAtual=0;
	  }
	  setPosAtual(posAtual);
	  tocaMusica();
	}
	
	
	//******************GRAVAR EXECUCOES NO BANCO************************
	private void registrarExecucao(){
		String nomeMusica = "";
		new CarregaMusicaTask().execute(nomeMusica);

	}
	
	   
	   // Executa a consulta em uma thead separada
	   private class CarregaMusicaTask extends AsyncTask<String, Object, Cursor>{
	      @Override
	      protected Cursor doInBackground(String... params){
	    	  Uri musicasURI = Uri.parse("content://"+ MusicasProvider.PROVIDER_NAME + "/musicas");//params[0]
		 	  ContentResolver cr = getContentResolver();
		 	  String selection = MusicasProvider.KEY_NOME + " = ?";
		 	  String nomeMusica = musicas.get(getPosAtual());
		 	  String[] selectionArgs = new String[] {nomeMusica};
		 	  Cursor cursor = cr.query(musicasURI, null, selection, selectionArgs, null);
      		  int nomeIndex = cursor.getColumnIndex("nome");
		 	  int reproducoesIndex = cursor.getColumnIndex("reproducoes");
		      ContentValues dados = new ContentValues();
		      ContentResolver crAdd = getContentResolver();
		      dados.put(MusicasProvider.KEY_NOME, nomeMusica);
		      

		 	  if(cursor.moveToFirst()){
		 		 int idIndex = cursor.getColumnIndex("_id");
		 		 int idLinha = cursor.getInt(idIndex);
		 		 int reproducoes = cursor.getInt(reproducoesIndex) + 1;
		 		 dados.put(MusicasProvider.KEY_REPRODUCOES, reproducoes);
		 		 Uri uri = Uri.parse("content://"+ MusicasProvider.PROVIDER_NAME + "/musicas/" + idLinha);            
	             cr.update(uri, dados, null,null);		 		 		 		 
		 	  }
		 	  else {
		 		 dados.put(MusicasProvider.KEY_REPRODUCOES, 1);
		         Uri uri = cr.insert(MusicasProvider.CONTENT_URI, dados); 
		 	  }
		      return cursor; //retorna a musica 
	      } 
	      // Usa o Cursor retornado do m�todo doInBackground
	      @Override
	      protected void onPostExecute(Cursor result) {
	         super.onPostExecute(result);
	         result.close();
	      } 
	   }	   
	   
}
