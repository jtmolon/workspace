package molon.joao.mediaplayer_joaomolon;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import molon.joao.mediaplayer_joaomolon.R;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListaMusicasFragment extends ListFragment {
	private static final String MEDIA_PATH = new String("/sdcard/musicas/");
	private List<String> musicas = new ArrayList<String>();
	
	OnMusicItemClickListener privateCallback;
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		privateCallback = (OnMusicItemClickListener) activity;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

		int layout = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? android.R.layout.simple_list_item_activated_1 : android.R.layout.simple_list_item_1;

		//setListAdapter(new ArrayAdapter<String>(getActivity(), layout, Lista_Imagens.nomes));
		atualizaListaMusicas();

	}
	
    public void atualizaListaMusicas() {
    	File home = new File(MEDIA_PATH);
    	if (home.listFiles( new Mp3Filter()).length > 0) {
    		for (File file : home.listFiles( new Mp3Filter())) {
    			musicas.add(file.getName());
    		}
		
    		ArrayAdapter<String> songList = new ArrayAdapter<String>(getActivity(), R.layout.list_item,musicas);
    		setListAdapter(songList);
		}
    }
	
	@Override
	public void onStart() {
		super.onStart();
		
		 if (getFragmentManager().findFragmentById(R.id.ListaMusicasFragmentId) != null) {
	            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
	        }
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		if (MainActivity.servico.getMusicas() == null) {
			MainActivity.servico.setMusicas(musicas);	
		}				
		FragmentManager fm = getFragmentManager();
		FragmentTransaction fragTransaction = fm.beginTransaction();
		fragTransaction.addToBackStack(null);
		privateCallback.onMusicItemClicked(position);
	    getListView().setItemChecked(position, true);
	}
	
	public ListaMusicasFragment() {
		// TODO Auto-generated constructor stub
	}

}
