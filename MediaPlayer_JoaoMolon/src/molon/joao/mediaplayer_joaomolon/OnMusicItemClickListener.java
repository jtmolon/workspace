package molon.joao.mediaplayer_joaomolon;

import java.util.List;

public interface OnMusicItemClickListener {

	public void onMusicItemClicked(int position);
	
}
