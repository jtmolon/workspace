package molon.joao.mediaplayer_joaomolon;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class MusicasProvider extends ContentProvider {

	public static final String PROVIDER_NAME = "molon.joao.MusicasProvider";
	public static final Uri CONTENT_URI = Uri.parse("content://"+ PROVIDER_NAME + "/musicas");
	      
	public static final String KEY_ROWID = "_id";
	public static final String KEY_NOME = "nome";
	public static final String KEY_REPRODUCOES = "reproducoes";
	
	private static final int MUSICAS = 1;
	private static final int MUSICA_ID = 2;   
	         
	private static final UriMatcher uriMatcher;
	static{
	   uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	   uriMatcher.addURI(PROVIDER_NAME, "musicas", MUSICAS);
	   uriMatcher.addURI(PROVIDER_NAME, "musicas/#", MUSICA_ID);      
	}	
	
	   //--- Para uso do banco de dados ---
	   private SQLiteDatabase musicasDB;

	   private static final String DATABASE_NAME = "MusicasBD";
	   private static final String DATABASE_TABLE = "musicas";
	   private static final int DATABASE_VERSION = 1;
	   private static final String CRIA_DATABASE = "create table musicas " +
	               "(_id integer primary key autoincrement, " +
			       " nome text not null, " +
	               " reproducoes int not null);" ;
		   
	   private static class DatabaseHelper extends SQLiteOpenHelper{
	      DatabaseHelper(Context context) {
		     super(context, DATABASE_NAME, null, DATABASE_VERSION);
		  }

		  @Override
		  public void onCreate(SQLiteDatabase db){
		     db.execSQL(CRIA_DATABASE);
		  }

		  @Override
		  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	         db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
		     onCreate(db);
		  }
	  }
	   
	public MusicasProvider() {

	}

	@Override
	public boolean onCreate() {
		Context context = getContext();
		DatabaseHelper dbHelper = new DatabaseHelper(context);
		musicasDB = dbHelper.getWritableDatabase();
		return (musicasDB == null)? false:true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		   SQLiteQueryBuilder sqlBuilder = new SQLiteQueryBuilder();
		   sqlBuilder.setTables(DATABASE_TABLE);
		           
		   if (uriMatcher.match(uri) == MUSICA_ID)
		      sqlBuilder.appendWhere(KEY_ROWID + " = " + uri.getPathSegments().get(1));                
		           
		      if (sortOrder==null || sortOrder=="")
		             sortOrder = KEY_REPRODUCOES;
		       
		      Cursor c = sqlBuilder.query(
		             musicasDB, 
		             projection, 
		             selection, 
		             selectionArgs, 
		             null, 
		             null, 
		             sortOrder);
		       
		      c.setNotificationUri(getContext().getContentResolver(), uri);
		      return c;
	}

	@Override
	public String getType(Uri uri) {
		   switch (uriMatcher.match(uri)){
	       //---get all books---
	       case MUSICAS:
	          return "vnd.android.cursor.dir/vnd.molon.joao.MediaPlayer_JoaoMolon.musicas";
	       //---get a particular book---
	       case MUSICA_ID:                
	          return "vnd.android.cursor.item/vnd.molon.joao.MediaPlayer_JoaoMolon.musicas";
	       default:
	          throw new IllegalArgumentException("Unsupported URI: " + uri);        
	       }

	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		   //insere um novo registro usando um ContentProvider
		   long idLinha = musicasDB.insert(DATABASE_TABLE, "", values);
		   
		   if (idLinha > 0){
		         Uri _uri = ContentUris.withAppendedId(CONTENT_URI, idLinha);
		         getContext().getContentResolver().notifyChange(_uri, null);    
		         return _uri;                
		      }        
		      throw new SQLException("Falha na inserção da linha na uri: " + uri);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
	      int count=0;
	      switch (uriMatcher.match(uri)){
	         case MUSICAS:
	            count = musicasDB.delete(DATABASE_TABLE,selection,selectionArgs);
	            break;
	         case MUSICA_ID:
	            String id = uri.getPathSegments().get(1);
	            count = musicasDB.delete(DATABASE_TABLE, 
	            		KEY_ROWID + " = " + id + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""),selectionArgs);
	            break;
	         default: throw new IllegalArgumentException("URI DESCONHECIDA " + uri);    
	      }       
	      getContext().getContentResolver().notifyChange(uri, null);
	      return count;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		   //atualiza um registro no cp
		   int count = 0;
		      switch (uriMatcher.match(uri)){
		         case MUSICAS:
		            count = musicasDB.update(
		               DATABASE_TABLE, 
		               values,
		               selection, 
		               selectionArgs);
		            break;
		         case MUSICA_ID:                
		            count = musicasDB.update(
		               DATABASE_TABLE, 
		               values,
		               KEY_ROWID + " = " + uri.getPathSegments().get(1) + 
		               (!TextUtils.isEmpty(selection) ? " AND (" + 
		                  selection + ')' : ""), 
		                selectionArgs);
		            break;
		         default: throw new IllegalArgumentException(
		            "Unknown URI " + uri);    
		      }       
		      getContext().getContentResolver().notifyChange(uri, null);
		      return count;

	}

}