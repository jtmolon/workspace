package molon.joao.questoescopa_joaomolon;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class QuestoesProvider extends ContentProvider {

	public static final String PROVIDER_NAME = "molon.joao.QuestoesProvider";
	public static final Uri CONTENT_URI = Uri.parse("content://"+ PROVIDER_NAME + "/questoes");
	      
	public static final String KEY_ROWID = "_id";
	public static final String KEY_QUESTAO = "questao";
	public static final String KEY_ALT1 = "alt1";
	public static final String KEY_ALT2 = "alt2";
	public static final String KEY_ALT3 = "alt3";
	public static final String KEY_ALTCORRETA = "altcorreta";
	public static final String KEY_NIVEL = "nivel";
	      
	private static final int QUESTOES = 1;
	private static final int QUESTAO_ID = 2;   
	         
	private static final UriMatcher uriMatcher;
	static{
	   uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	   uriMatcher.addURI(PROVIDER_NAME, "questoes", QUESTOES);
	   uriMatcher.addURI(PROVIDER_NAME, "questoes/#", QUESTAO_ID);      
	}

   //--- Para uso do banco de dados ---
   private SQLiteDatabase questoesDB;

   private static final String DATABASE_NAME = "QuestoesBD";
   private static final String DATABASE_TABLE = "questoes";
   private static final int DATABASE_VERSION = 1;
   private static final String CRIA_DATABASE = "create table questoes " +
               "(_id integer primary key autoincrement, " +
               " questao text not null," +
               " alt1 text not null," +
               " alt2 text not null," +
               " alt3 text not null," +
               " altcorreta int not null," +
               " nivel int not null);" ;
	   
   private static class DatabaseHelper extends SQLiteOpenHelper{
      DatabaseHelper(Context context) {
	     super(context, DATABASE_NAME, null, DATABASE_VERSION);
	  }

	  @Override
	  public void onCreate(SQLiteDatabase db){
	     db.execSQL(CRIA_DATABASE);
	  }

	  @Override
	  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
         db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
	     onCreate(db);
	  }
  }
	
	
	
	public QuestoesProvider() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onCreate() {
		   Context context = getContext();
		   DatabaseHelper dbHelper = new DatabaseHelper(context);
		   questoesDB = dbHelper.getWritableDatabase();
		   return (questoesDB == null)? false:true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		   //recebe uma consulta do cliente e retorna um cursor
		   
		   SQLiteQueryBuilder sqlBuilder = new SQLiteQueryBuilder();
		   sqlBuilder.setTables(DATABASE_TABLE);
		           
		   if (uriMatcher.match(uri) == QUESTAO_ID)
		   //---if getting a particular book---
		      sqlBuilder.appendWhere(KEY_ROWID + " = " + uri.getPathSegments().get(1));                
		           
		      if (sortOrder==null || sortOrder=="")
		             sortOrder = KEY_QUESTAO;
		       
		      Cursor c = sqlBuilder.query(
		             questoesDB, 
		             projection, 
		             selection, 
		             selectionArgs, 
		             null, 
		             null, 
		             sortOrder);
		       
		      //---register to watch a content URI for changes---
		      c.setNotificationUri(getContext().getContentResolver(), uri);
		      return c;
	}

	@Override
	public String getType(Uri uri) {
		   switch (uriMatcher.match(uri)){
	       //---get all books---
	       case QUESTOES:
	          return "vnd.android.cursor.dir/vnd.molon.joao.QuestoesCopa.questoes";
	       //---get a particular book---
	       case QUESTAO_ID:                
	          return "vnd.android.cursor.item/vnd.com.exemplos.QuestoesCopa.questoes";
	       default:
	          throw new IllegalArgumentException("Unsupported URI: " + uri);        
	       }  
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		   //insere um novo registro usando um ContentProvider
		   long idLinha = questoesDB.insert(DATABASE_TABLE, "", values);
		   
		   if (idLinha > 0){
		         Uri _uri = ContentUris.withAppendedId(CONTENT_URI, idLinha);
		         getContext().getContentResolver().notifyChange(_uri, null);    
		         return _uri;                
		      }        
		      throw new SQLException("Falha na inserção da linha na uri: " + uri);
	}

	@Override
	public int delete(Uri uri, String selecao, String[] valores) {
		   //exclui um registro no cp
	      // arg0 = uri 
	      // arg1 = selection
	      // arg2 = selectionArgs
	      int count=0;
	      switch (uriMatcher.match(uri)){
	         case QUESTOES:
	            count = questoesDB.delete(DATABASE_TABLE,selecao,valores);
	            break;
	         case QUESTAO_ID:
	            String id = uri.getPathSegments().get(1);
	            count = questoesDB.delete(DATABASE_TABLE, KEY_ROWID + " = " + id + (!TextUtils.isEmpty(selecao) ? " AND (" + selecao + ')' : ""),valores);
	            break;
	         default: throw new IllegalArgumentException("URI DESCONHECIDA " + uri);    
	      }       
	      getContext().getContentResolver().notifyChange(uri, null);
	      return count;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		   //atualiza um registro no cp
		   int count = 0;
		      switch (uriMatcher.match(uri)){
		         case QUESTOES:
		            count = questoesDB.update(
		               DATABASE_TABLE, 
		               values,
		               selection, 
		               selectionArgs);
		            break;
		         case QUESTAO_ID:                
		            count = questoesDB.update(
		               DATABASE_TABLE, 
		               values,
		               KEY_ROWID + " = " + uri.getPathSegments().get(1) + 
		               (!TextUtils.isEmpty(selection) ? " AND (" + 
		                  selection + ')' : ""), 
		                selectionArgs);
		            break;
		         default: throw new IllegalArgumentException(
		            "Unknown URI " + uri);    
		      }       
		      getContext().getContentResolver().notifyChange(uri, null);
		      return count;
	}

}
