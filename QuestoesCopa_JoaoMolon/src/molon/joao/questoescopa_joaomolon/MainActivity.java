package molon.joao.questoescopa_joaomolon;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.os.Build;

public class MainActivity extends ListActivity {
	public static final String LINHA_ID = "idLinha"; 
	private ListView questoesListView; //
	private CursorAdapter questoesAdapter; // Adaptador para a ListView

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_main);

		questoesListView = getListView(); 
		questoesListView.setOnItemClickListener(viewQuestoesListener);      

		// mapeia cada coluna da tabela com um componente da tela
		String[] origem = new String[]{"questao"};
		int[] destino = new int[] {R.id.txtQuestao};
		int flags = 0;
		      
		questoesAdapter = new SimpleCursorAdapter(MainActivity.this,R.layout.activity_viewquestoes,null,origem,destino,flags);
		setListAdapter(questoesAdapter); // set contactView's adapter		
		
		//if (savedInstanceState == null) {
		//	getSupportFragmentManager().beginTransaction()
		//			.add(R.id.container, new PlaceholderFragment()).commit();
		//}
	}

    //Quando o usu�rio clica em uma linha da consulta, uma nova inten��o
    //� exeutada, para exibir os dados do livro selecionado 
    OnItemClickListener viewQuestoesListener = new OnItemClickListener(){
       public void onItemClick(AdapterView<?> parent, View view, int posicao,long id){
          Intent viewQuestoes = new Intent(MainActivity.this, ViewQuestoes.class);
          viewQuestoes.putExtra(LINHA_ID, id);
          startActivity(viewQuestoes); 
       } // end method onItemClick
    }; // end viewContactListener	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
	       Intent addNovoLivro = new Intent(MainActivity.this, AddQuestao.class);
	       startActivity(addNovoLivro);
	       return super.onOptionsItemSelected(item);
	}

	
	protected void onResume(){
	//sempre que executar onResume, ir� fazer uma busca no banco de dados
	//e vai atualizar a tela de exibi��o dos livros cadastrados
	   super.onResume(); 
	   new ObtemQuestoes().execute((Object[]) null);
	} 
////////////////////////////////////////////////////////////
	// Quando precisamos dos resultados de uma opera��o do BD na thread da 
	// interface gr�fica, vamos usar AsyncTask para efetuar a opera��o em
	// uma thread e receber os resultados na thread da interface gr�fica
	private class ObtemQuestoes extends AsyncTask<Object, Object, Cursor>{
	      @Override
	      protected Cursor doInBackground(Object... params){
	    	 Uri questoes = Uri.parse("content://"+ QuestoesProvider.PROVIDER_NAME + "/questoes");
	 	     ContentResolver cr = getContentResolver();
	 	     Cursor cursor = cr.query(questoes, null, null, null, "questao desc");
	         return cursor; //retorna todos os livros 
	      } // end method doInBackground

	      // use the Cursor returned from the doInBackground method
	      @Override
	      protected void onPostExecute(Cursor result){
	    	  questoesAdapter.changeCursor(result); //altera o cursor para um novo cursor
	      } // end method onPostExecute
	   } // end class GetContactsTask
	
	
	
	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

}
