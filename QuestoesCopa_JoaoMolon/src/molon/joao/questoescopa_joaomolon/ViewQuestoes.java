package molon.joao.questoescopa_joaomolon;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.os.Build;

public class ViewQuestoes extends Activity {
	private long idLinha; 
	private TextView lblQuestao;
	private TextView lblAlt1;
	private TextView lblAlt2;
	private TextView lblAlt3;
	private TextView lblAltCorreta;
	private TextView lblNivel;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_questoes);

		lblQuestao = (TextView) findViewById(R.id.lblQuestao);
		lblAlt1 = (TextView) findViewById(R.id.lblAlt1);
		lblAlt2 = (TextView) findViewById(R.id.lblAlt2);
		lblAlt3 = (TextView) findViewById(R.id.lblAlt3);
		lblAltCorreta = (TextView) findViewById(R.id.lblAltCorreta);
		lblNivel = (TextView) findViewById(R.id.lblNivel);
		
		Bundle extras = getIntent().getExtras();
		idLinha = extras.getLong(MainActivity.LINHA_ID);
		
		//if (savedInstanceState == null) {
		//	getSupportFragmentManager().beginTransaction()
		//			.add(R.id.container, new PlaceholderFragment()).commit();
	//	}
	}

	   @Override
	   protected void onResume(){
	      super.onResume();
	      new CarregaQuestaoTask().execute(idLinha);
	   }	
	
	   // Executa a consulta em uma thead separada
	   private class CarregaQuestaoTask extends AsyncTask<Long, Object, Cursor>{
	      @Override
	      protected Cursor doInBackground(Long... params){
	    	  Uri questoes = Uri.parse("content://"+ QuestoesProvider.PROVIDER_NAME + "/questoes/"+idLinha);//params[0]
		 	  ContentResolver cr = getContentResolver();
		 	  Cursor cursor = cr.query(questoes, null, null, null, null);
		      return cursor; //retorna o livro 
	      } 
	      // Usa o Cursor retornado do m�todo doInBackground
	      @Override
	      protected void onPostExecute(Cursor result) {
	         super.onPostExecute(result);
	         result.moveToFirst(); 
	         
	         int questaoIndex = result.getColumnIndex("questao");
	         int alt1Index = result.getColumnIndex("alt1");
	         int alt2Index = result.getColumnIndex("alt2");
	         int alt3Index = result.getColumnIndex("alt3");
	         int altcorretaIndex = result.getColumnIndex("altcorreta");	         
	         int nivelIndex = result.getColumnIndex("nivel");

	         lblQuestao.setText(result.getString(questaoIndex));
	         lblAlt1.setText(result.getString(alt1Index));
	         lblAlt2.setText(result.getString(alt2Index));
	         lblAlt3.setText(result.getString(alt3Index));
	         lblAltCorreta.setText(result.getString(altcorretaIndex));
	         lblNivel.setText(result.getString(nivelIndex));
	         result.close(); 
	      } 
	   } 
	   
	   
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_viewquestoes, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
	      switch (item.getItemId()){
	         case R.id.editItem:
	            Intent addEditQuestao = new Intent(this, AddQuestao.class);
	            
	            addEditQuestao.putExtra(MainActivity.LINHA_ID, idLinha);
	            addEditQuestao.putExtra("questao", lblQuestao.getText());
	            addEditQuestao.putExtra("alt1", lblAlt1.getText());
	            addEditQuestao.putExtra("alt2", lblAlt2.getText());
	            addEditQuestao.putExtra("alt3", lblAlt3.getText());
	            addEditQuestao.putExtra("altcorreta", lblAltCorreta.getText());
	            addEditQuestao.putExtra("nivel", lblNivel.getText());
	 
	            startActivity(addEditQuestao); 
	            return true;
	         case R.id.deleteItem:
	            deleteLivro(); 
	            return true;
	         default:
	            return super.onOptionsItemSelected(item);
	      }
	}
	
	   private void deleteLivro(){
		      
		      AlertDialog.Builder builder = new AlertDialog.Builder(ViewQuestoes.this);

		      builder.setTitle(R.string.confirmaTitulo); 
		      builder.setMessage(R.string.confirmaMensagem); 

		      // provide an OK button that simply dismisses the dialog
		      builder.setPositiveButton(R.string.botao_delete,
		         new DialogInterface.OnClickListener(){
		            public void onClick(DialogInterface dialog, int button){

		               // create an AsyncTask that deletes the contact in another 
		               // thread, then calls finish after the deletion
		               AsyncTask<Long, Object, Object> deleteTask =
		                  new AsyncTask<Long, Object, Object>(){
		                     @Override
		                     protected Object doInBackground(Long... params){
		                    	 try{
		                    		 ContentResolver cr = getContentResolver();
		                    		 Uri uri = Uri.parse("content://"+ QuestoesProvider.PROVIDER_NAME + "/questoes/" + idLinha); //params[0]           
		                             cr.delete(uri, null, null);
		                    	 }
		                         catch(SQLException e){
		                        	 e.printStackTrace();
		                         }
		                        return null;
		                     } // end method doInBackground

		                     @Override
		                     protected void onPostExecute(Object result){
		                        finish(); 
		                     } // end method onPostExecute
		                  }; // end new AsyncTask

		               // execute the AsyncTask to delete contact at rowID
		               deleteTask.execute(new Long[] { idLinha });               
		            } // end method onClick
		         } // end anonymous inner class
		      ); // end call to method setPositiveButton
		      
		      builder.setNegativeButton(R.string.botao_cancel, null);
		      builder.show(); // display the Dialog
		   } // end method deleteContact	

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_view_questoes,
					container, false);
			return rootView;
		}
	}

}
