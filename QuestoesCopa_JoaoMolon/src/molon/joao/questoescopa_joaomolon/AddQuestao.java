package molon.joao.questoescopa_joaomolon;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.os.Build;

public class AddQuestao extends Activity {
    private long idLinha; 
    private EditText txtQuestao;
    private EditText txtAlt1;
    private EditText txtAlt2;
    private EditText txtAlt3;
    private RadioButton rbAlt1;
    private RadioButton rbAlt2;
    private RadioButton rbAlt3;   
    private RadioButton rbNivel1;
    private RadioButton rbNivel2;
    private RadioButton rbNivel3;
    private RadioButton rbNivel4;
    private RadioButton rbNivel5;
    private Button btSalvar;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_questao);

		txtQuestao = (EditText) findViewById(R.id.txtQuestao);
		txtAlt1 = (EditText) findViewById(R.id.txtAlt1);
		txtAlt2 = (EditText) findViewById(R.id.txtAlt2);
		txtAlt3 = (EditText) findViewById(R.id.txtAlt3);
		rbAlt1 = (RadioButton) findViewById(R.id.rbAlt1);
		rbAlt2 = (RadioButton) findViewById(R.id.rbAlt2);
		rbAlt3 = (RadioButton) findViewById(R.id.rbAlt3);
		rbNivel1 = (RadioButton) findViewById(R.id.rbNivel1);
		rbNivel2 = (RadioButton) findViewById(R.id.rbNivel2);
		rbNivel3 = (RadioButton) findViewById(R.id.rbNivel3);
		rbNivel4 = (RadioButton) findViewById(R.id.rbNivel4);
		rbNivel5 = (RadioButton) findViewById(R.id.rbNivel5);
		btSalvar = (Button) findViewById(R.id.btSalvar);	
		
        Bundle extras = getIntent().getExtras(); // get Bundle of extras

	    // Se há extras, usa os valores para preencher a tela
	    if (extras != null){
	       idLinha = extras.getLong("idLinha");
	       txtQuestao.setText(extras.getString("questao"));  
	       txtAlt1.setText(extras.getString("alt1"));  
	       txtAlt2.setText(extras.getString("alt2"));
	       txtAlt3.setText(extras.getString("alt3"));
	       int altCorreta = Integer.parseInt(extras.getString("altcorreta"));
	       switch (altCorreta) {
	       		case 1: rbAlt1.setChecked(true); break;
	       		case 2: rbAlt2.setChecked(true); break;
	       		case 3: rbAlt3.setChecked(true); break;			
	       }
	       int nivel = Integer.parseInt(extras.getString("nivel"));
	       switch (nivel) {
      			case 1: rbNivel1.setChecked(true); break;
      			case 2: rbNivel2.setChecked(true); break;
      			case 3: rbNivel3.setChecked(true); break;			
      			case 4: rbNivel4.setChecked(true); break;
      			case 5: rbNivel5.setChecked(true); break;
	       }
	       
	    } // end if
	    
	    btSalvar = (Button) findViewById(R.id.btSalvar);
	    btSalvar.setOnClickListener(salvarQuestaoButtonClicked);
		
		//if (savedInstanceState == null) {
		//	getSupportFragmentManager().beginTransaction()
		//			.add(R.id.container, new PlaceholderFragment()).commit();
		//}
	}

	   OnClickListener salvarQuestaoButtonClicked = new OnClickListener(){
		      public void onClick(View v){
		         if (txtQuestao.getText().length() != 0){
		            AsyncTask<Object, Object, Object> salvaQuestaoTask = new AsyncTask<Object, Object, Object>(){
		                  @Override
		                  protected Object doInBackground(Object... params){
		                     salvaQuestao(); // Salva o livro na base de dados
		                     return null;
		                  } // end method doInBackground
		      
		                  @Override
		                  protected void onPostExecute(Object result){
		                     finish(); // Fecha a atividade
		                  } 
		               };
		               
		            // Salva o livro no BD usando uma thread separada
		            salvaQuestaoTask.execute((Object[]) null); 
		         } // end if
		         else {
		            // Cria uma caixa de di�logo 
		            AlertDialog.Builder builder = new AlertDialog.Builder(AddQuestao.this);
		            builder.setTitle("Erro ao adicionar a questão"); 
		            builder.setMessage("Ocorreu um erro inesperado ao salvar a questão");
		            builder.setPositiveButton("OK", null); 
		            builder.show(); 
		         } 
		      } 
		   };	
	
		   // Salva a questao na base de dados
		   private void salvaQuestao(){ 
		      try{
		         ContentValues dados = new ContentValues();
		         ContentResolver cr = getContentResolver();
		         dados.put(QuestoesProvider.KEY_QUESTAO, txtQuestao.getText().toString());
		         dados.put(QuestoesProvider.KEY_ALT1,txtAlt1.getText().toString());
		         dados.put(QuestoesProvider.KEY_ALT2,txtAlt2.getText().toString());
		         dados.put(QuestoesProvider.KEY_ALT3,txtAlt3.getText().toString());
		         int altCorreta = 0;
		         if (rbAlt1.isChecked()) {
					altCorreta = 1;
				}else if (rbAlt2.isChecked()) {
		        	 altCorreta = 2;					
				}else if (rbAlt3.isChecked()){
					altCorreta = 3;
				}
		         dados.put(QuestoesProvider.KEY_ALTCORRETA, altCorreta);
		         int nivel = 0;
		         if (rbNivel1.isChecked()) {
					nivel = 1;
				}else if (rbNivel2.isChecked()) {
					nivel = 2;
				}else if (rbNivel3.isChecked()) {
					nivel = 3;
				}else if (rbNivel4.isChecked()) {
					nivel = 4;
				}else if (rbNivel5.isChecked()) {
					nivel = 5;
				}
			
		         dados.put(QuestoesProvider.KEY_NIVEL, nivel);
		    	  
		         if (getIntent().getExtras() == null){
		             Uri uri = cr.insert(QuestoesProvider.CONTENT_URI, dados);
		         } 
		         else{
		             Uri uri = Uri.parse("content://"+ QuestoesProvider.PROVIDER_NAME + "/questoes/" + idLinha);            
		             cr.update(uri, dados, null,null);
		         }
		      }catch(SQLException e){
		    	 e.printStackTrace();
		      }
		   }		   
		   
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_questao, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_add_questao,
					container, false);
			return rootView;
		}
	}

}
